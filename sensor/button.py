#!/usr/bin/env python
 
import time
import os.path
import serial
import PWM



def parity_bit(hex_list):
    invert = lambda x: int(bin((~x  & 0xff) + 1 & 0xff),2)
    check_sum = sum(map(invert,hex_list))
    return check_sum & 0xff

class Button:
    GPIO_RESET    = False;  # Whether GPIOs should be re-exported
    GPIO_PATH     = "/sys/class/gpio";
    GPIO_SW_DIR  = "in";
    GPIO_VAL_HI   = "1";
    GPIO_VAL_LO   = "0";
    GPIO_SW1 = "11";     #GPIO2_I003 
    GPIO_SW2 = "132";     #GPIO2_I002   1*32+2  = 34
    GPIO_SW = ""
    press_cycles = 0
    hold = False
    pressed = False

    def __init__(self, gpio_button):
        if "SW1" == gpio_button:
            self.GPIO_SW  = self.GPIO_SW1
            self.init_butt(self.GPIO_SW1)

        if "SW2" == gpio_button:
            self.GPIO_SW  = self.GPIO_SW2
            self.init_butt(self.GPIO_SW2)

    def init_butt(self, gpio_button):
        # Open GPIO export & unexport files
        exportFile = open(self.GPIO_PATH+'/export', 'w')
        # Unexport GPIO if it exists and GPIO_RESET is enabled
        exportExistsA = os.path.isdir(self.GPIO_PATH+'/gpio'+ gpio_button)
        if not exportExistsA:
            exportFile.write(gpio_button)
            exportFile.flush()
         
        dirFile_A  = open(self.GPIO_PATH+'/gpio'+gpio_button+'/direction','w')
        dirFile_A.write(self.GPIO_SW_DIR)
        dirFile_A.flush()

    def status(self, gpio_button): 
        fileObject  = open(self.GPIO_PATH+'/gpio'+gpio_button+'/value','r')
        fileObject.flush()
        fileContent = fileObject.read()
        fileObject.close()
        return True if 0 == int(fileContent) else False

    def listen(self):

        p = self.status(self.GPIO_SW)
        if p and not self.hold:
            self.press_cycles += 1
            self.hold = True
            self.pressed = False

        elif p and self.hold:
            self.press_cycles += 1

        elif not p and self.hold:
            self.hold = False
            self.press_cycles = 0 
            self.pressed = True

        elif not p and not self.hold:
            self.pressed = False

def task(p, i):
    print("pump power", i*10, " %")
    d = period_in_ns*i
    p.duty_cycle(d)
    p.enable(1)
    # Sleep for blink off duration

period_in_ns = 300000
def main():
    print("Starting button")
    sw1 = Button("SW1")
    sw2 = Button("SW2")
    proc = 5
    p = PWM.pwm()
    print('W1')
    p.period(period_in_ns*10)
    print(period_in_ns*10)
    print('W2')
    p.enable(0)
    print('W3')
    p.duty_cycle(0)
    
    while True:
        sw1.listen()
        sw2.listen()

        if sw1.pressed:
            print("PRESSED! 1")
            proc += 1
            if proc > 10:
                proc = 10
            task(p, proc)

            time.sleep(0.05)

        if sw2.pressed:
            print("PRESSED! 2")
            proc -= 1
            if proc < 0:
                proc = 0

            task(p, proc)
            time.sleep(0.05)

        time.sleep(0.01)

if __name__ == "__main__":
    main()

