#!/usr/local/bin/python2.7
# 22 04 00 01 00 0D 67 5C
# 22 04 1A 4D 43 37 34 30 20 4D 75 6C 74 69 66 75 6E 63 74 4D 43 30 30 35 31 30 36 00 69 0A 92
# 22 04 00 02 00 08 57 5F
# buffer = [0x22, 0x04, 0x00, 0x01, 0x00, 0x0D]

# ===============================================================


def CalCRC16(data):
    #print(data, length) #Print data, length
    crc = 0xFFFF
    length = len(data)
    if length == 0:
       length = 1
    j = 0
    while length != 0:
        crc ^= list.__getitem__(data, j)
        #print('j=0x%02x, length=0x%02x, crc=0x%04x' %(j,length,crc))
        for i in range(0, 8):
            if crc & 1:
                crc >>= 1
                crc ^= 0xA001
            else:
                crc >>= 1
        length -= 1
        j += 1
    return crc
# ===============================================================


def CRCBuffer(buffer):
    crc_transformation = CalCRC16(buffer)
    #crc_calculation = hex(crc_transformation)
    #print('crc_calculation:',crc_calculation)
    tasd = [0x00, 0x00]
    tasd[0] = crc_transformation & 0xFF
    tasd[1] = (crc_transformation >> 8) & 0xFF
    H = hex(tasd[0])
    L = hex(tasd[1])
    H_value = int(H, 16)
    L_value = int(L, 16)
    buffer.append(H_value)
    buffer.append(L_value)
    return buffer
# ===============================================================


#if __name__ == '__main__':
#
#    print(CRCBuffer(buffer))
