#!/usr/bin/env python
 
import time
import os.path
from threading import Thread

GPIO_RESET    = False;  # Whether GPIOs should be re-exported
GPIO_PATH     = "/sys/class/gpio";
GPIO_DIR_OUT  = "out";
GPIO_DIR_IN  = "in";
GPIO_VAL_HI   = "1";
GPIO_VAL_LO   = "0";
GPIO_CHAN_LED1 = "35";   #GPIO2_I003 
GPIO_CHAN_LED2 = "34";   #GPIO2_I002   1*32+2 = 34
GPIO_CHAN_LED5 = "106";  #GPIO2_I003   3*32+10=106
GPIO_CHAN_SW1 = "132";   #GPIO5_I004   4*32+4 =132 
GPIO_CHAN_SW2 =  "11";   #GPI01_I011 
 
BLINK_PERIOD  = 500;  # Blink period (milliseconds)
BLINK_DUTY    = 0.25; # Blink duty cycle (fraction)
 
class Thread_LED(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.setDaemon(True)
        self.running = True

    def run(self):
        print('running read_input')
        while self.running:
            #time.sleep(1)
          # run_animation(ani, koliko, poz, utrip)
            run_animation(1, 5, 1, 1)

    def stop(self):
        self.running = False

class LED():
    val_LED1 = None
    val_LED2 = None
    val_LED5 = None
    def __init__(self): 
        print('start')
        ### Initialize GPIO - optionally reset if already initialized
 
        ## Note: GPIOs which are already used in the drivers can not be controlled from sysfs, 
        ## unless a driver explicitly exported that particular pins GPIO.
 
        # Open GPIO export & unexport files
        exportFile = open(GPIO_PATH+'/export', 'w')
        print(exportFile)
        unexportFile = open(GPIO_PATH+'/unexport', 'w')
        print(unexportFile)
        # Unexport GPIO if it exists and GPIO_RESET is enabled
        exportExists1 = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_LED1)
        if exportExists1 and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_LED1)
            unexportFile.flush()
        
        exportExists2 = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_LED2)
        if exportExists2 and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_LED2)
            unexportFile.flush()

        
        exportExists5 = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_LED5)
        if exportExists5 and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_LED5)
            unexportFile.flush()    
 
        # Export GPIO
        if not exportExists1 or GPIO_RESET:
            exportFile.write(GPIO_CHAN_LED1)
            exportFile.flush()

        if not exportExists2 or GPIO_RESET:
            exportFile.write(GPIO_CHAN_LED2)
            exportFile.flush()  

        if not exportExists5 or GPIO_RESET:
            exportFile.write(GPIO_CHAN_LED5)
            exportFile.flush()      
 

        # Open GPIO direction file to set direction
        dirFileLED1  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_LED1+'/direction','w')
        dirFileLED2  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_LED2+'/direction','w')
        dirFileLED5  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_LED5+'/direction','w')
 
        # Set GPIO direction to "out"
        dirFileLED1.write(GPIO_DIR_OUT)
        dirFileLED1.flush()
        dirFileLED2.write(GPIO_DIR_OUT)
        dirFileLED2.flush()
        dirFileLED5.write(GPIO_DIR_OUT)
        dirFileLED5.flush()

 
        # Open GPIO value file to set value
        self.val_LED1 = open(GPIO_PATH+'/gpio'+GPIO_CHAN_LED1+'/value','w')
        self.val_LED2 = open(GPIO_PATH+'/gpio'+GPIO_CHAN_LED2+'/value','w')
        self.val_LED5 = open(GPIO_PATH+'/gpio'+GPIO_CHAN_LED5+'/value','w')
        # Loop indefinitely
    
    def LED1_ON(self):
        self.val_LED1.write(GPIO_VAL_HI)
        self.val_LED1.flush()
    def LED1_OFF(self):
        self.val_LED1.write(GPIO_VAL_LO)
        self.val_LED1.flush()
    
    def LED2_ON(self):
        self.val_LED2.write(GPIO_VAL_HI)
        self.val_LED2.flush()
    def LED2_OFF(self):
        self.val_LED2.write(GPIO_VAL_LO)
        self.val_LED2.flush()

    def LED5_ON(self):
        self.val_LED5.write(GPIO_VAL_HI)
        self.val_LED5.flush()
    def LED5_OFF(self):
        self.val_LED5.write(GPIO_VAL_LO)
        self.val_LED5.flush() 

class BUTTOM():
    val_SW1 = None
    val_SW2 = None
    def __init__(self): 
        print('start')
        ### Initialize GPIO - optionally reset if already initialized
 
        ## Note: GPIOs which are already used in the drivers can not be controlled from sysfs, 
        ## unless a driver explicitly exported that particular pins GPIO.
 
        # Open GPIO export & unexport files
        exportFile = open(GPIO_PATH+'/export', 'w')
        unexportFile = open(GPIO_PATH+'/unexport', 'w')

        # Unexport GPIO if it exists and GPIO_RESET is enabled
        exportExists1 = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_SW1)
        if exportExists1 and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_SW1)
            unexportFile.flush()
        
        exportExists2 = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_SW2)
        if exportExists2 and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_SW2)
            unexportFile.flush()

   
 
        # Export GPIO
        if not exportExists1 or GPIO_RESET:
            exportFile.write(GPIO_CHAN_SW1)
            exportFile.flush()

        if not exportExists2 or GPIO_RESET:
            exportFile.write(GPIO_CHAN_SW2)
            exportFile.flush()  
 

        # Open GPIO direction file to set direction
        dirFileSW1  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_SW1+'/direction','w')
        dirFileSW2  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_SW2+'/direction','w')

 
        # Set GPIO direction to "in"
        dirFileSW1.write(GPIO_DIR_IN)
        dirFileSW1.flush()
        dirFileSW2.write(GPIO_DIR_IN)
        dirFileSW2.flush()

 
        # Open GPIO value file to set value
        self.val_SW1 = open(GPIO_PATH+'/gpio'+GPIO_CHAN_SW1+'/value','r')
        self.val_SW2 = open(GPIO_PATH+'/gpio'+GPIO_CHAN_SW2+'/value','r')

    
    def SW1(self):
        s =''
        self.val_SW1.seek(0)
        s = self.val_SW1.read()
        return s

    def SW2(self):
        s =''
        self.val_SW2.seek(0)
        s = self.val_SW2.read()
        return s



#animacija 1: preiÅ¾ge
def animacija1(ledica,pavza2,dolga,koliko):
     for i in range(koliko):
        time.sleep(pavza2)
        ledica.LED1_ON()
        time.sleep(pavza2)
        ledica.LED1_OFF()
        time.sleep(pavza2)
        ledica.LED2_ON()
        time.sleep(pavza2)
        ledica.LED2_OFF()
        time.sleep(pavza2)
        ledica.LED5_ON()
        time.sleep(pavza2)
        ledica.LED5_OFF()
        time.sleep(dolga) 
   

#animacija 2: izbiranje poljubnje animacije med 1,2 in 5
def animacija2(ledica,poz,utrip,pavza,dolga):
  if poz == 1:
    ledica.LED1_OFF()
    ledica.LED2_ON()
    ledica.LED5_ON()
    time.sleep(pavza)

  elif poz == 2:
    ledica.LED2_OFF()
    ledica.LED1_ON()
    ledica.LED5_ON()
    time.sleep(pavza)

         
  elif poz == 5:
    ledica.LED5_OFF()
    ledica.LED1_ON()
    ledica.LED2_ON()
    time.sleep(pavza)

  for i in range(utrip):
    if poz == 1:
      ledica.LED1_ON()
      time.sleep(pavza)
      ledica.LED1_OFF()
      time.sleep(pavza)

    elif poz == 2:
      ledica.LED2_ON()
      time.sleep(pavza)
      ledica.LED2_OFF()
      time.sleep(pavza)
    elif poz == 5:
      ledica.LED5_ON()
      time.sleep(pavza)
      ledica.LED5_OFF()
      time.sleep(pavza)
  ledica.LED1_OFF()
  ledica.LED2_OFF()
  ledica.LED5_OFF()
  time.sleep(dolga)

#animacija 3: preiÅ¾ge ledice eno za drugo
def animacija3(ledica,pavza2,dolga,koliko):
    for i in range(koliko):
        time.sleep(pavza2)
        ledica.LED1_ON()
        time.sleep(pavza2)
        ledica.LED1_OFF()
        time.sleep(pavza2)
        ledica.LED2_ON()
        time.sleep(pavza2)
        ledica.LED2_OFF()
        time.sleep(pavza2)
        ledica.LED5_ON()
        time.sleep(pavza2)
        ledica.LED5_OFF()

        time.sleep(pavza2)
        ledica.LED5_ON()
        time.sleep(pavza2)
        ledica.LED5_OFF()
        time.sleep(pavza2)
        ledica.LED2_ON()
        time.sleep(pavza2)
        ledica.LED2_OFF()
        time.sleep(pavza2)
        ledica.LED1_ON()
        time.sleep(pavza2)
        ledica.LED1_OFF()

    time.sleep(dolga)   


#animacija4
def animacija4(ledica,pavza2,dolga,koliko):
    for i in range(koliko):
        time.sleep(pavza2)
        ledica.LED1_ON()
        time.sleep(pavza2)
        ledica.LED2_ON()
        ledica.LED1_OFF()
        time.sleep(pavza2)
        ledica.LED2_ON()
        time.sleep(pavza2)
        ledica.LED5_ON()
        ledica.LED2_OFF()
        time.sleep(pavza2)
        ledica.LED5_ON()
        time.sleep(pavza2)
        ledica.LED5_OFF()  


def run_animation( ani, koliko, poz, utrip):
  led = LED()
  #pavze  
  pavza = 0.2
  pavza2 = 0.2
  dolga = 0.5  

  while 1:

    if ani == 1:  
        animacija1(led,pavza2,dolga,koliko)
    elif ani == 2:
        animacija2(led,poz,utrip,pavza,dolga)
    elif ani == 3:
        animacija3(led,pavza2,dolga,koliko)
    elif ani == 4:
        animacija4(led,pavza2,dolga,koliko)

  return

def main():
  led = LED()
  #pavze  
  pavza = 0.2
  pavza2 = 0.2
  dolga = 0.5  

  while 1:
    #klicanje kombinacij
    ani = int(input("Izberi animacijo od 1 do 4: "))
    if ani == 1:  
        koliko = int(input("Animacija1: kolikokrat: "))
        animacija1(led,pavza2,dolga,koliko)
    elif ani == 2:
        poz = int(input("Katera Ledica 1,2 ali 5: "))
        utrip = int(input("Kolikokrat naj Ledica utripne: "))
        animacija2(led,poz,utrip,pavza,dolga)
    elif ani == 3:
        koliko = int(input("Animacija1: kolikokrat: "))        
        animacija3(led,pavza2,dolga,koliko)
    elif ani == 4:
        koliko = int(input("Animacija1: kolikokrat: "))      
        animacija4(led,pavza2,dolga,koliko)

  return
 
if __name__ == "__main__":
    main()
