import sys
import time
from threading import Thread
import modbus_client
#import modbus_server
import store
import init
import uart_mux
#import client_reg_manager 
#from init import ini
#from init import print_com_param
#from init import out_ser
#from client_reg_manager import out_reg
#from client_reg_manager import list_reg
from constants import *
from server import ModbusServer
import socket 
import server
from store import *
if IMX6 and LED :
    import Led_animation
#from pyModbusTCP.server import ModbusServer, DataBank


class Terminal_Read(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.setDaemon(True)
        self.running = True

    def run(self):
        print('running read_input')
        while self.running:
            #time.sleep(1)
            Term_Read()

    def stop(self):
        self.running = False


def Term_Read():
    #print('Start Read_inp')
    #while True:
    try:
        commandFull = input("COMMAND > ")

        commandParts = commandFull.split()
        if len(commandParts) == 0:
           return None

        command = commandParts[0]
        args = commandParts[1:]

        #print(command)

        if command == "?":
            print('clientstop \t\t ustavi delovanje branje senzorjev')
            print('clientrun  \t\t zazene delovanje branje senzorjev')
            #print('mer        \t\t izpise prebrane vrednosti registrov senzorjev')
            print(
                't          \t\t prikaze promet branje registrov senzorjev v enem ciklu')
            print('com?       \t\t parametri komunikacije')
            print('reg?       \t\t registri senzorjev ')
            print('outreg     \t\t generate registers setting file -> register.txt')
            print('loadreg    \t\t reload registers setting from file <- register.csv')
            print('lstreg <start> <end>>   list register from <start> to <end>')
            print('outser     \t\t generate serial settings file -> serial.txt')
            print('loadser    \t\t reload serial setting from file <- serial.csv')
            print('lstser      \t\t list serial settings>')
            print('outmer     \t\t generate measuring results file -> meas.txt')

        elif command == "clientstop":
            print('execute command stop')
            #f_Mod_Client.stop()

        elif command == "clientrun":
            print('execute command stop')
            #f_Mod_Client.run()

        #elif command == "mer":
        #    modbus_client.izpis_mer_po_sezorjih()

        elif command == "t":
            store.print_trafic = 1

        elif command == "lstser":
            init.lst_ser()

        elif command == "loadser":
            init.import_serial_settings("serial.csv")

        elif command == "outreg":
            init.out_reg("reg_test.csv")

        elif len(command) == 0:
            exit

        elif command == "echo":
            #print(args)
            for d in range(40):
                print("%d = %s" % (d, str(store.meas_data[d])))
        else:
            try:
                print(eval(commandFull))
            except Exception as e:
                print(e)
#print("Unknown command: ", command)

    except KeyboardInterrupt:
        print("Type exit to exit the program")
        sys.exit(1)




class Server_ModBus(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.setDaemon(True)

        self.run_flag = True

    def run(self):
        print('running modb_server')
        #DataBank_change = False
        #hostname = socket.gethostname()
        #print(hostname)
        #ser_ip = socket.gethostbyname(hostname)
        ser_port = 502
        #print(ser_ip)
        ser = ModbusServer("0.0.0.0", no_block=True)
        print("Star server...")
        ser.start()
        print("Server is on line")

        a = [0, 1, 2]
        len(a)

        a.pop()

        len(a)
        state = [0]
        while self.run_flag:
            if(ser.is_run):
                if(DataBank.is_any_change()):
                    DataBank.clr_change()
                    print("Bila je sprememba")
            else:
                print("Restart server...")
                ser.start()
                print("Server is on line")
            time.sleep(1)
            #modbus_server.modb_server()

        print('end of modb_server')

    def stop(self):
        print('I am going to stop modb_server')
        self.run_flag = False

class Client_ModBus(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.setDaemon(True)
        #print('running mdb_cliet')
        self.run_flag = True


    def run(self):
        print('running mdb_cliet')
        #if modbus_client.ini_store_data():
        while self.run_flag:
           
            #print(self.run_flag)
            modbus_client.modb_client()
            time.sleep(1)
        #else:
        #    print('end of mdb_cliet')

    def stop(self):
        print('I am going to stop modb_client')
        self.run_flag = False


class Client_ModBus(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.setDaemon(True)
        #print('running mdb_cliet')
        self.run_flag = True

    def run(self):
        print('running mdb_cliet')
        #if modbus_client.ini_store_data():
        while self.run_flag:

            #print(self.run_flag)
            modbus_client.modb_client()
            time.sleep(1)
        #else:
        #    print('end of mdb_cliet')

    def stop(self):
        print('I am going to stop modb_client')
        self.run_flag = False



init.ini()

#print(store.settings)
if IMX6 and LED:
    f_Led = Led_animation.Thread_LED()
f_Term_Read = Terminal_Read()
f_Mod_Server = Server_ModBus()
f_Mod_Client = Client_ModBus()

#TODO: rethink start stop threads?
f_Term_Read.start()
f_Mod_Server.start()
f_Mod_Client.start()
if IMX6 and LED :
    f_Led.start()

while True:
    pass

Print('Kar nehal sem ?')

"""

        elif command == "outser":
            out_ser('serial.txt')

        elif command == "outmer":
            out_mer('meas.txt')
                    elif command == "com?":
            print_com_param()

        elif command == "reg?":
            modbus_client.izpis_registrov()

        elif command == "outreg":
            out_reg('register.txt')

        elif command == "loadreg":
            init.import_client_reg("registers.csv")

        elif command == "lstreg":
            if len(commandParts) == 3:
                start = int(commandParts[1])
                end = int(commandParts[2])
                list_reg(start, end)
            else:
                print('no <start> or <end> parameters')
"""
