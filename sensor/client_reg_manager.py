import serial
import struct
import sys
import CRC16
import time
import csv
import logging
import store
#import modbus_client
#import modbus_server
from constants import *









"""

def fill_Client_reg_memory():
    #fill with 0
    for reg in range(0,1000):
        store.client_reg_mem.append ( 0 )

    for reg in range (100,800):
        if( reg in store.settings):
            typ = store.settings[reg][1]
            if(typ in ['UInt16', 'Int16','Int8']):
                store.client_reg_mem[reg-100] = int(store.settings[reg][0])
            elif(type in ['UInt32','Int32']):
                store.ser_mem[reg-100] = int(((store.settings[reg][0]) >> 16) & 0xFFFF)
                store.ser_mem[(reg-100) + 1] = int((store.settings[reg][0]) & 0xFFFF)
            elif (typ in ['Float32', 'Float64']):   
                val = float(store.settings[reg][0])
                a = list(struct.pack('<f', val))
                store.client_reg_mem[reg-100]   = a[3]*256 + a[2]
                store.client_reg_mem[(reg+1)-100] = a[1]*256 + a[0]
            else:
                txt = store.settings[reg][0]
                if len(txt) % 2 == 1:
                    txt += "\0"
                for ind in range (0,len(txt)>>1):
                    store.client_reg_mem[(reg + ind)-100] = ord(txt[(ind*2)]) * 256 + ord(txt[(ind*2)+1])


def fill_serial_reg_memory():
    #fill with 0
    for reg in range(0,40):
        store.ser_mem.append ( 0 )

    for reg in range (2000,2040):
        if( reg in store.settings):
            typ = store.settings[reg][1]
            if(typ in ['UInt16', 'Int16','Int8']):
                store.ser_mem[reg-2000] = int(store.settings[reg][0])
            elif(typ in ['Char']):
                # int(store.settings[reg][0])
                store.ser_mem[reg - 2000] = ord(store.settings[reg][0])
            elif (typ in ['UInt32', 'Int32']):
                store.ser_mem[reg-2000] = int(((store.settings[reg][0]) >> 16) & 0xFFFF)
                store.ser_mem[(reg-2000) + 1] = int((store.settings[reg][0]) & 0xFFFF)
            elif (typ in ['Float32', 'Float64']):   
                val = float(store.settings[reg][0])
                a = list(struct.pack('<f', val))
                store.ser_mem[(reg-2000)]   = a[3]*256 + a[2]
                store.ser_mem[(reg-2000)+1] = a[1]*256 + a[0]
            else:
                txt = store.settings[reg][0]
                if len(txt) % 2 == 1:
                    txt += "\0"
                for ind in range (0,len(txt)>>1):
                    store.ser_mem[(reg - 2000) + ind] = ord(txt[(ind*2)]) * 256 + ord(txt[(ind*2)+1])

"""

def out_reg(csv_file_path):
    with open(csv_file_path, 'w') as csvfile:
        for  row in store.settings:
            if (row < 100) or (row > 1000):
                continue
            txt = ("{0:>5}; {1:>16}; {2:>10}; {3:>12}\r".format(
                row, store.settings[row][0], store.settings[row][1], store.settings[row][2]))
            csvfile.write(txt )
    csvfile.close()
    print('file ->' + csv_file_path)


def list_reg(start,end):
    for row in range (start,end):
        if row in store.settings:
            print("{0:>5}; {1:>16}; {2:>10}; {3:>12}\r".format(row, store.settings[row][0], store.settings[row][1], store.settings[row][2]))
            #csvfile.write(txt)
    



"""
def fill_serial_reg_memory():
    #fill with 0
    for reg in range(0,40):
        store.ser_mem.append ( 0 )

    for ser in range (0,1):
        add = 20 * ser
        
        ch = 0  #32 bit baudrate
        if( 1000+add+ch in store.settings):
            store.ser_mem [add+ch] = int(((store.settings[1000+add+ch]) >> 16) & 0xFFFF)
            store.ser_mem[add+ch+1] = int((store.settings[1000+add+ch]) & 0xFFFF)
        
        ch = 2  #char
        if( 1000+add+ch in store.settings):
            store.ser_mem [add+ch] = ord(store.settings[1000+add+ch])
                
        for ch in [3,4]: #16bit 
            if( 1000+add+ch in store.settings):
                store.ser_mem [add+ch] = int(store.settings[1000+add+ch])

        for ch in [5,7]: #Float 
            if(1000+add+ch in store.settings):
                val = float(store.settings[1000+add+ch])
                a = list(struct.pack('<f', val))
                store.ser_mem[add+ch]   = a[3]*256 + a[2]
                store.ser_mem[add+ch+1] = a[1]*256 + a[0]

"""
# read from memory array -> client_reg_mem
def get_Client_reg_val( adr, len):
    answ = []
    adr = adr - 100
    for i in range (len):
        answ.append((store.client_reg_mem[adr+i] >> 8) & 0xFF)
        answ.append(store.client_reg_mem[adr+i] & 0xFF)
    return True, answ

# read from memory array -> ser_mem
def get_Client_ser_val( adr, len): 
    answ = []
    adr = adr - 2000
    for i in range (len):
        answ.append((store.ser_mem[adr+i] >> 8) & 0xFF)
        answ.append(store.ser_mem[adr+i] & 0xFF)
    return True, answ



"""
def test_reg():
    for reg in range(100, 300):
        answ = store.client_reg_mem[reg-100]
        print(answ)
        pass
    print(set_Client_reg(1, [0, 2], 2))
    print(set_Client_reg(120, [0, 2], 2))
    print(set_Client_reg(141, [0, 3], 2))
    print(set_Client_reg(100, [0, 2], 2))
    print(set_Client_reg(200, [0, 2], 2))
    print(set_Client_reg(301, [0, 2], 2))
    print(set_Client_reg(106, [60, 135, 252, 185], 4))
    print(set_Client_reg(107, [60, 0, 2, 1], 4))
    print(set_Client_reg(108, [68, 154, 82, 37], 4))
    print(set_Client_reg(110, [97, 98, 99, 100, 101, 102], 6))
    print(set_Client_reg(112, [97, 98, 99, 100, 101, 102], 6))
"""

"""
# se ne dela !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def get_Client_reg( reg ,len):
    #fill with 0
    if not ((reg > 99)  and (reg < 300)):
        return False
    
    reg = reg -100
    if not (reg in store.settings):
        return False
    
    mod = reg % 20
    if( mod in [0,1,2,3,4,5]):
        val = int(store.settings[reg])
        return [
            val // 256,
            val % 256
        ]
    elif( mod in [6,8]):
        val = float(store.settings[reg])
        a = list(struct.pack('<f', val))
        return [ int(a[3]), int(a[2]),int(a[1]),int(a[0])]
    elif (mod > 9) and ( mod < 17):
       pass
"""
