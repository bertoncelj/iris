import struct
import socket
import modbus_client
from threading import Lock, Thread
from constants import *

TOTAL_TYPE_8 = 0xf8
FLOAT32 = 0x44
FLOAT64 = 0x88
SINT16 = 0x12
UINT16 = 0x02
SINT8 = 0x11
UINT8 = 0x01

STRING = 0xff

"""
R_ADD = "add"
R_FUN = "fun"
R_SIZE = "size"
R_TYPE = "type"
R_REG = "reg"
R_NAME = "name"
R_LOC = "loc"
"""
#                     12;           2;       UInt16;    Save = 1, Reload =2, Restart = 3
#                     20;        sensor Hub; String32;  Description
# DataBank.settings[int(row[0])] = val,      row[2],    row[3]
#
# 
# DataBank.meas[address] = 0x1234    --> 30000 meas
# 
# DataBank.holding[address] = 0x1234  --> 40000 settings
# UInt32
#
# Float32
#               0  1  2  3     2 3 0 1
# 14C0, 41B7   14 C0 41 B7   0x41b714c0 -> 22.8851318359
#
#
#
#

lst_reg = [ 1, 3, ]
version = "b.1.006"
HW_version = "1.01"
#      123.4567                    123456792.0
meas_data = [0x42, 0xF6, 0xE9, 0xE0,   0x4C,
             0xEB, 0x79, 0xA3]*32  # address 0 ... 63
meas_status = [0x00, 0x01]*32   # address 64 ... 64+32 95
ser_mem = []
sensor_status = [0,0,0,0,0,0,0,0]

client_reg_mem = []
print_trafic = 0
save_settings_flag = 0
average = {0: 0}

loc_value = {PPM:     [STRUCT_PPM, LOC_AVR_PPM, LOC_FST_PPM, LOC_OFFSET_PPM],
             VOLTAGE: [STRUCT_VOLTAGE, LOC_AVR_VOLTAGE, LOC_FST_VOLTAGE, LOC_OFFSET_VOLTAGE],
             CURRENT: [STRUCT_CURRENT, LOC_AVR_CURRENT, LOC_FST_CURRENT, LOC_OFFSET_CURRENT],
             CURRENT_ZERO: [STRUCT_CURRENT_ZERO, LOC_AVR_CURRENT_ZERO, LOC_FST_CURRENT_ZERO, LOC_OFFSET_CURRENT_ZERO],
             TEMPERATURE:  [STRUCT_TEMPERATURE, LOC_AVR_TEMPERATURE, LOC_FST_TEMPERATURE, LOC_OFFSET_TEMPERATURE],
                }



class ser_client:
    baudrate = 19200
    parity = 'E'
    stopbits = 1
    bytesize = 8
    timeout = 0.6
    inter_byte_timeout = 0.1


class ser_server:
    baudrate = 19200
    parity = 'E'
    stopbits = 1
    bytesize = 8
    timeout = 0.6
    inter_byte_timeout = 0.1


class DataBank:

    """ Data class for thread safe access to bits and words space """


    words_lock = Lock()
    holding = [0] * 1000 #40000 settings
    meas =  [0] * 2000  #30000
    DataBank_change = False
    
    settings = {0: 0}



    @classmethod
    def get_words_3(cls, address, number=1):  # READ_HOLDING_REGISTERS 40000
        """Read data on server words space
        40000
        :param address: start address
        :type address: int
        :param number: number of words (optional)
        :type number: int
        :returns: list of int or None if error
        :rtype: list or None
        """
        # secure extract of data from list used by server thread
        with cls.words_lock:
            if (address >= 0) and (address + number <= len(cls.holding)):
                return cls.holding[address: number + address]
            else:
                return None


    @classmethod
    def set_words_3(cls, address, word_list):  # SET_HOLDING_REGISTERS 
        """Write data to server words space
        40000
        :param address: start address
        :type address: int
        :param word_list: a list of word to write
        :type word_list: list
        :returns: True if success or None if error
        :rtype: bool or None
        :raises ValueError: if word_list members cannot be convert to int
        """
        # ensure word_list values are int with a max bit length of 16
        word_list = [int(w) & 0xffff for w in word_list]
        # secure copy of data to list used by server thread
        with cls.words_lock:
            if (address >= 0) and (address + len(word_list) <= len(cls.holding)):
                cls.holding[address: address + len(word_list)] = word_list
                cls.DataBank_change = True
                #modbus_client.check_imported_address(address)
                return True
            else:
                return None


    @classmethod
    def get_words_4(cls, address, number=1):  # Read Input Registers (0x04)
        """Read MEASUREments 30000
        """
        # secure extract of data from list used by server thread
        with cls.words_lock:
            if (address >= 0) and (address + number <= len(cls.meas)):
                return cls.meas[address: number + address]
            else:
                return None

    @classmethod
    def set_bytes_4(cls, address, byts_list):  # Write to Input Registers (0x04)
        #Write MEASUREments 30000
        # ensure word_list values are int with a max bit length of 16
        #word_list = [int(w) & 0xffff for w in word_list]
        
        # secure copy of data to list used by server thread
        ln = int(len(byts_list)/2)
        word_list = [0]*ln
        with cls.words_lock:
            if (address >= 0) and (address + ln <= len(cls.meas)):
                for i in range (ln):
                    word_list[i] = (byts_list[i*2] << 8) + byts_list[(i*2)+1]
                cls.meas[address: address + len(word_list)] = word_list
                return True
            else:
                return None

    
    @classmethod
    def set_words_4(cls, address, word_list):  # Write to Input Registers (0x04)
        #Write MEASUREments 30000
        # ensure word_list values are int with a max bit length of 16
        #word_list = [int(w) & 0xffff for w in word_list]

        # secure copy of data to list used by server thread
        with cls.words_lock:
            if (address >= 0) and (address + len(word_list) <= len(cls.meas)):
                cls.meas[address: address + len(word_list)] = word_list
                return True
            else:
                return None
            
    @classmethod
    def write_to_memory(cls, address, word_list):
        """Write data to server words space

        :param address: start address
        :type address: int
        :param word_list: a list of word to write
        :type word_list: list
        :returns: True if success or None if error
        :rtype: bool or None
        :raises ValueError: if word_list members cannot be convert to int
        """
        # ensure word_list values are int with a max bit length of 16
        word_list = [int(w) & 0xffff for w in word_list]
        # secure copy of data to list used by server thread
        with cls.words_lock:
            if (address >= 0) and (address + len(word_list) <= len(cls.holding)):
                cls.holding[address: address + len(word_list)] = word_list
                cls.DataBank_change = True
                return True
            else:
                return None


    @classmethod
    def set_Client_reg(cls, reg, data):
        ln = len(data)
        if not(reg in cls.settings):
            return False
        type = cls.settings[reg][1]
        if(type in ["UInt16", "UInt8","Int16", "Int8"]) and (ln == 2):
            val = data[0]*256 + data[1]
            cls.settings[reg] = (val, cls.settings[reg][1], cls.settings[reg][2])
            return True
        elif(type in ["UInt32", "Int32"]) and (ln == 4):
            val = (data[0]*256*256*256) + (data[1]*256*256) + (data[2]*256) + data[3]
            cls.settings[reg] = (val, cls.settings[reg][1], cls.settings[reg][2])
            return True
        elif(type in ["Float32"]) and (ln == 4):
            aa = bytearray(data)
            #store.settings[reg][0] = struct.unpack('<f', aa)
            #fl = struct.unpack('<f', aa)
            fl = struct.unpack('>f', aa)[0]
            cls.settings[reg] = (fl, cls.settings[reg][1], cls.settings[reg][2])
            return True
        
        elif (type in ["String16"]) and (ln <= 16):
            txt = ""
            for i in data:
                txt += chr(data[i])

            cls.settings[reg] = (txt, cls.settings[reg][1], cls.settings[reg][2])
            return True
        elif (type in ["Char"]) and (ln == 2):
            txt = ""
            txt += chr(data[1])

            cls.settings[reg] = (txt, cls.settings[reg]
                                 [1], cls.settings[reg][2])
            return True
        else:
            return False

    
    @classmethod
    def fill_Client_reg_memory(cls):
 
        for reg in range( 1, 400):
            if(reg in cls.settings):
                typ = cls.settings[reg][1]
                if(typ in ['UInt16', 'Int16', 'Int8']):
                    cls.set_words_3(reg, [int(cls.settings[reg][0]) & 0xFFFF])
                    #store.client_reg_mem[reg - 100] = int(store.settings[reg][0])
                elif(typ in ['UInt32', 'Int32']):
                    cls.set_words_3( reg+1, [int(((cls.settings[reg][0]) >> 16) & 0xFFFF)])
                    cls.set_words_3(   reg, [int((cls.settings[reg][0]) & 0xFFFF)])
                
                elif (typ in ['Float32', 'Float64']):  #Littel endian 
                    val = float(cls.settings[reg][0])
                    a = list(struct.pack('<f', val))
                    cls.set_words_3(reg+1, [a[3]*256 + a[2]])
                    cls.set_words_3(reg,   [a[1]*256 + a[0]])
                else:
                    txt = cls.settings[reg][0]
                    if len(txt) % 2 == 1:
                        txt += "\0"
                    for ind in range(0, len(txt) >> 1):
                        cls.set_words_3(
                            reg+ind, [ord(txt[(ind*2)]) * 256 + ord(txt[(ind*2)+1])])


    @classmethod
    def set_serial_reg(cls,reg, data, len):
        if not ((reg > 999) and (reg < 1025)):
            return False
        reg = reg - 2000
        mod = reg % 20

        if mod == 0 and (len == 4):
            val = data[0]*256*256*256 + data[1]*256*256 + data[2]*256 + data[3]
            cls.settings[reg] = (val, cls.settings[reg]
                                [1], cls.settings[reg][2])
            return True

        elif(mod in [2, 3, 4]) and (len == 2):
            val = data[0]*256 + data[1]
            cls.settings[reg] = (val, cls.settings[reg]
                                [1], cls.settings[reg][2])
            return True

        else:
            return False

    @classmethod
    def fill_serial_reg_memory(cls):  #L

        for reg in range(200, 240):
            if(reg in cls.settings):
                typ = cls.settings[reg][1]
                if(typ in ['UInt16', 'Int16', 'Int8']):
                    cls.set_words_3(reg, [int(cls.settings[reg][0])])
                elif(typ in ['Char']):
                    # int(store.settings[reg][0])
                    cls.set_words_3(reg, [ord(cls.settings[reg][0])])
                elif (typ in ['UInt32', 'Int32']):
                    cls.set_words_3(   reg, [int((cls.settings[reg][0]) & 0xFFFF)])
                    cls.set_words_3( reg+1, [int(((cls.settings[reg][0]) >> 16) & 0xFFFF)])

                elif (typ in ['Float32', 'Float64']):
                    val = float(cls.settings[reg][0])
                    a = list(struct.pack('<f', val))
                    cls.set_words_3(reg, [a[3]*256 + a[2]])
                    cls.set_words_3(reg+1, [a[1]*256 + a[0]])
                else:
                    txt = cls.settings[reg][0]
                    if len(txt) % 2 == 1:
                        txt += "\0"
                    for ind in range(0, len(txt) >> 1):
                        cls.set_words_3(
                            reg+ind, [ord(txt[(ind*2)]) * 256 + ord(txt[(ind*2)+1])])


    @classmethod
    def is_any_change(cls):
        return cls.DataBank_change

    @classmethod
    def clr_change(cls):
        cls.DataBank_change = False

    @classmethod
    def set_holding_string(cls,loc,txt):
        if len(txt) % 2 == 1:
            txt += "\0"
        for ind in range(0, len(txt) >> 1):
            cls.set_words_4(loc+ind, [ord(txt[(ind*2)]) * 256 + ord(txt[(ind*2)+1])])



    #@classmethod
    #def check_imported_address(address):
    #    if address == 60 :
    #        save_settings_flag = 1
    #pass


