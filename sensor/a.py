import serial

ser = serial.Serial('/dev/ttyUSB0', 9600)

def parity_bit(hex_list):
    invert = lambda x: int(bin((~x  & 0xff) + 1 & 0xff),2)
    check_sum = sum(map(invert,hex_list))
    return check_sum & 0xff

# plan
class Sensor:
    # Call commenads
    # Switch to passiv upload
    c_info  = [0x01, 0x78, 0x41, 0x00, 0x00, 0x00, 0x00]
    # Running light status
    c_light_status  = [0x01, 0x8a, 0x00, 0x00, 0x00, 0x00, 0x00]
    c_1 = [0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00]
    d_2 = [0xd2]

    def __init__(self):
        pass

    def read_measurement(self):
        pass

    def read_info(self):
        pass

    def read_ppm(self):
        get_array = self.read_sensor(self.c_1)
        value = get_array[6]*256 + get_array[7]
        return value

    def read_temp(self):
        get_array = self.read_sensor(self.d_2,4,False)
        temp = (get_array[0]* 256 + get_array[1])*0.01
        hum = (get_array[2]* 256 + get_array[3])*0.01
        return temp, hum

    def read_led_status(self):
        return self.read_sensor(self.c_light_status)

    def read_sensor(self, command, len_recive=9, parity=True):
        if parity == True:
            command = [0xff] + command + [parity_bit(command)]
        print("Send:", command)
        ser.write(bytearray(command))
        data = ser.read(len_recive)
        data_hex = data.hex()
        return [int(data_hex[i:i+2],16) for i in range(0, len(data_hex),2)]


s0 = Sensor()
print(s0.read_ppm())
print(s0.read_temp())











# c = 0xff0178400000000047
# c1 = b"\xff\x00\x87\x00\x00\x00\x00\x00\x79"
# c = b"\x18\x00\xcb\x02\x00\x00\x00\x00\x35"
# a = b"\x\x00\xcb\x02\x00\x00\x00\x00\x35"
# a = [0xff, 0x01, 0x78, 0x40, 0x00, 0x00, 0x00, 0x00]
# a = [ 0xd7, 0x18, 0x00, 0xc8, 0x02, 0x01, 0x00]
# a = [ 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00]
# a = [ 0x86, 0x00, 0x2a, 0x00, 0x00, 0x00, 0x20]
# a = [0x01, 0x78, 0x41, 0x00, 0x00, 0x00, 0x00]
# a = [0xff, 0xd7, 0x78, 0x41, 0x00, 0x00, 0x00, 0x00]
# a = [0x0a, 0x09, 0x11, 0xf4]
# a = [0x18, 0, 0xcb, 0x02, 0, 0,0,0]


# hx_s = lambda x: "{:02x}".format(x)
# s_l = "x"+"x".join(list(map(hx_s, a)))
# print(s_l)
# bb = int(s_l,16)
# ser.write(s_l)
# a =22
# b = a.to_bytes(1,'big')
# print(b)


