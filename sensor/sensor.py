import serial

ser = serial.Serial('/dev/ttyUSB0', 9600)

def parity_bit(hex_list):
    invert = lambda x: int(bin((~x  & 0xff) + 1 & 0xff),2)
    check_sum = sum(map(invert,hex_list))
    return check_sum & 0xff

# plan
class Sensor:
    # Call commenads
    # Switch to passiv upload
    c_info  = [0x01, 0x78, 0x41, 0x00, 0x00, 0x00, 0x00]
    # Running light status
    c_light_status  = [0x01, 0x8a, 0x00, 0x00, 0x00, 0x00, 0x00]
    c_1  =    [0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00]

    def __init__(self):
        pass

    def read_measurement(self):
        pass

    def read_info(self):
        pass

    def read_led_status(self):
        return self.read_sensor(self.c_light_status)

    def read_sensor(self, command):
        a = [0xff] + command + [parity_bit(command)]
        print("Send:", a)
        ser.write(bytearray(a))
        data = ser.read(9)
        data_hex = data.hex()
        return [int(data_hex[i:i+2],16) for i in range(0, len(data_hex),2)]

