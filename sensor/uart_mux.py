#!/usr/bin/env python
 
import time
import os.path
import serial



def parity_bit(hex_list):
    invert = lambda x: int(bin((~x  & 0xff) + 1 & 0xff),2)
    check_sum = sum(map(invert,hex_list))
    return check_sum & 0xff


# plan
class Sensor:
    # Call commenads
    # Switch to passiv upload
    c_info  = [0x01, 0x78, 0x41, 0x00, 0x00, 0x00, 0x00]

    # Switch to active upload
    active_upload  = [0x01, 0x78, 0x40, 0x00, 0x00, 0x00, 0x00]

    # Running light status
    c_light_status  = [0x01, 0x8a, 0x00, 0x00, 0x00, 0x00, 0x00]
    c_1  =    [0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00]
    d_2 = [0xd2]


    @classmethod
    def __init__(self, serial):
        print("init", serial)
        self.serial = serial
        print("serial", self.serial)

    @classmethod
    def read_measurement(self):
        pass

    @classmethod
    def read_info(self):
        pass

    @classmethod
    def read_ppm(self):
        get_array = self.read_sensor(self.c_1)
        if get_array == "Empty":
            return "Empty"
        print(get_array)
        value = get_array[6]*256 + get_array[7]
        return value

    @classmethod
    def read_temp(self):
        get_array = self.read_sensor(self.d_2,4,False)
        if get_array == "Empty":
            return "Empty"
        temp = (get_array[0]* 256 + get_array[1])*0.01
        hum = (get_array[2]* 256 + get_array[3])*0.01
        return temp, hum
    
    @classmethod
    def read_led_status(self):
        return self.read_sensor(self.c_light_status)

    @classmethod
    def read_sensor(self, command, len_recive=9, parity=True):
        if parity == True:
            command = [0xff] + command + [parity_bit(command)]
        print("Send:", command)
        print(serial)
        self.serial.write(bytearray(command))
        reentry = 500
        while(len_recive > 0):
            recived_bytes = self.serial.inWaiting() 
            if (reentry > 0):
                reentry -= 1
            else:
                break
            if (recived_bytes > 0):
                len_recive -= recived_bytes
                data = self.serial.read(recived_bytes) 
            time.sleep(0.001) 

        data_hex = data.hex()
        if len(data_hex) == 0:
            return "Empty"
        else:
            return [int(data_hex[i:i+2],16) for i in range(0, len(data_hex),2)]




GPIO_RESET    = False;  # Whether GPIOs should be re-exported
GPIO_PATH     = "/sys/class/gpio";
GPIO_DIR_OUT  = "out";
GPIO_DIR_IN  = "in";
GPIO_VAL_HI   = "1";
GPIO_VAL_LO   = "0";
GPIO_CHAN_A = "35";     #GPIO2_I003 
GPIO_CHAN_B = "34";     #GPIO2_I002   1*32+2  = 34
GPIO_CHAN_EN1 = "36";   #GPIO2_I004   1*32+4  = 36
GPIO_CHAN_EN2 = "52";   #GPIO2_I020   1*32+20 = 52 
GPIO_CHAN_EN3 = "53";   #GPI02_I021   1*32+21 = 51

# GPIO Mosfet 
# SODIM 184 _> GPIO4_IO10 -> 106 TA MORA BIT ON
# SODIM 188 -> GIO4_IO15 -> 111
# SODIM 186 -> GIO4_IO11-> 107

# sw1 -> GPIO5_IO04  132
# sw2 -> GPIO1_IO11  11
GPIO_CHAN_3_3V = "106"
 
""" 
class Thread_LED(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.setDaemon(True)
        self.running = True

    def run(self):
        print('running read_input')
        while self.running:
            #time.sleep(1)
          # run_animation(ani, koliko, poz, utrip)
            run_animation(1, 5, 1, 1)

    def stop(self):
        self.running = False
"""

class UART_MUX():
    val_A = None
    val_B = None
    val_EN1 = None
    val_EN2 = None
    val_EN3 = None
    
    @classmethod
    def init(self): 
        print('start')
        ### Initialize GPIO - optionally reset if already initialized
 
        ## Note: GPIOs which are already used in the drivers can not be controlled from sysfs, 
        ## unless a driver explicitly exported that particular pins GPIO.
 
        # Open GPIO export & unexport files
        exportFile = open(GPIO_PATH+'/export', 'w')
        print(exportFile)
        unexportFile = open(GPIO_PATH+'/unexport', 'w')
        print(unexportFile)
        # open 3.3 source voltage mosfet for onboard
        exportExists3_3 = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_3_3V)
        if exportExists3_3 and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_3_3V)
            unexportFile.flush()
        
        # Unexport GPIO if it exists and GPIO_RESET is enabled
        exportExistsA = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_A)
        if exportExistsA and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_A)
            unexportFile.flush()
        
        exportExistsB = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_B)
        if exportExistsB and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_B)
            unexportFile.flush()

        
        exportExistsEN1 = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_EN1)
        if exportExistsEN1 and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_EN1)
            unexportFile.flush()    
        
        exportExistsEN2 = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_EN2)
        if exportExistsEN2 and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_EN2)
            unexportFile.flush()    
        
        exportExistsEN3 = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_EN3)
        if exportExistsEN3 and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_EN3)
            unexportFile.flush() 
        
        # Export GPIO
        if not exportExistsA or GPIO_RESET:
            exportFile.write(GPIO_CHAN_A)
            exportFile.flush()

        if not exportExistsB or GPIO_RESET:
            exportFile.write(GPIO_CHAN_B)
            exportFile.flush()  

        if not exportExistsEN1 or GPIO_RESET:
            exportFile.write(GPIO_CHAN_EN1)
            exportFile.flush()      
        
        if not exportExistsEN2 or GPIO_RESET:
            exportFile.write(GPIO_CHAN_EN2)
            exportFile.flush()
        
        if not exportExistsEN3 or GPIO_RESET:
            exportFile.write(GPIO_CHAN_EN3)
            exportFile.flush()

        if not exportExists3_3 or GPIO_RESET:
            exportFile.write(GPIO_CHAN_3_3V)
            exportFile.flush()

        # Open GPIO direction file to set direction
        dirFile_A  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_A+'/direction','w')
        dirFile_B  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_B+'/direction','w')
        dirFile_EN1  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_EN1+'/direction','w')
        dirFile_EN2  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_EN2+'/direction','w')
        dirFile_EN3  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_EN3+'/direction','w')
        dirFile_3_3V  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_3_3V+'/direction','w')
 
        # Set GPIO direction to "out"
        dirFile_A.write(GPIO_DIR_OUT)
        dirFile_A.flush()
        dirFile_B.write(GPIO_DIR_OUT)
        dirFile_B.flush()
        dirFile_EN1.write(GPIO_DIR_OUT)
        dirFile_EN1.flush()
        dirFile_EN2.write(GPIO_DIR_OUT)
        dirFile_EN2.flush()
        dirFile_EN3.write(GPIO_DIR_OUT)
        dirFile_EN3.flush()
        dirFile_3_3V.write(GPIO_DIR_OUT)
        dirFile_3_3V.flush()
 
        # Open GPIO value file to set value
        self.val_A = open(GPIO_PATH+'/gpio'+GPIO_CHAN_A+'/value','w')
        self.val_B = open(GPIO_PATH+'/gpio'+GPIO_CHAN_B+'/value','w')
        self.val_EN1 = open(GPIO_PATH+'/gpio'+GPIO_CHAN_EN1+'/value','w')
        self.val_EN2 = open(GPIO_PATH+'/gpio'+GPIO_CHAN_EN2+'/value','w')
        self.val_EN3 = open(GPIO_PATH+'/gpio'+GPIO_CHAN_EN3+'/value','w')
        self.val_3_3V = open(GPIO_PATH+'/gpio'+GPIO_CHAN_3_3V+'/value','w')
        
        # open 3v3 on 
        self.val_3_3V.write(GPIO_VAL_HI)
        self.val_3_3V.flush()
        # Loop indefinitely
    
    @classmethod
    def A_ON(self):
        self.val_A.write(GPIO_VAL_HI)
        self.val_A.flush()

    @classmethod
    def A_OFF(self):
        self.val_A.write(GPIO_VAL_LO)
        self.val_A.flush()
    
    @classmethod
    def B_ON(self):
        self.val_B.write(GPIO_VAL_HI)
        self.val_B.flush()
    
    @classmethod
    def B_OFF(self):
        self.val_B.write(GPIO_VAL_LO)
        self.val_B.flush()

    @classmethod
    def EN1_ON(self):
        self.val_EN1.write(GPIO_VAL_HI)
        self.val_EN1.flush()
    
    @classmethod
    def EN1_OFF(self):
        self.val_EN1.write(GPIO_VAL_LO)
        self.val_EN1.flush() 
    
    @classmethod
    def EN2_ON(self):
        self.val_EN2.write(GPIO_VAL_HI)
        self.val_EN2.flush()

    @classmethod
    def EN2_OFF(self):
        self.val_EN2.write(GPIO_VAL_LO)
        self.val_EN2.flush()
    
    @classmethod
    def EN3_ON(self):
        self.val_EN3.write(GPIO_VAL_HI)
        self.val_EN3.flush()
    
    @classmethod
    def EN3_OFF(self):
        self.val_EN3.write(GPIO_VAL_LO)
        self.val_EN3.flush()

    @classmethod
    def set_mux_uart (self,pos):
        if(pos==1):
            self.A_OFF()
            self.B_OFF() 
            self.EN1_OFF()
            self.EN2_ON()
            self.EN3_ON()
        elif(pos==2):
            self.A_ON()
            self.B_OFF() 
            self.EN1_OFF()
            self.EN2_ON()
            self.EN3_ON()    
        elif(pos==3):
            self.A_OFF()
            self.B_ON() 
            self.EN1_OFF()
            self.EN2_ON()
            self.EN3_ON()
        elif(pos==4):
            self.A_ON()
            self.B_ON() 
            self.EN1_OFF() 
            self.EN2_ON()
            self.EN3_ON()
        elif(pos==5):
            self.A_OFF()
            self.B_OFF() 
            self.EN1_ON()
            self.EN2_OFF()
            self.EN3_ON()
        elif(pos==6):
            self.A_ON()
            self.B_OFF() 
            self.EN1_ON()
            self.EN2_OFF()
            self.EN3_ON()
        elif(pos==7):
            self.A_OFF()
            self.B_ON() 
            self.EN1_ON()
            self.EN2_OFF()
            self.EN3_ON()
        elif(pos==8):
            self.A_ON()
            self.B_ON() 
            self.EN1_ON()
            self.EN2_OFF()
            self.EN3_ON()
        elif(pos==9):
            self.A_OFF()
            self.B_OFF() 
            self.EN1_ON()
            self.EN2_ON()
            self.EN3_OFF()
# default 
        else:   
            self.A_OFF()
            self.B_OFF() 
            self.EN1_OFF()
            self.EN2_ON()
            self.EN3_ON()


def fast_uart():
    # ser = serial.Serial( port='/dev/ttyUSB0',)
    ser = serial.Serial( port='/dev/ttymxc1',)
    ser.baudrate = 9600
    ser.parity = 'N' 
    ser.stopbits = 1
    ser.bytesize = 8
    ser.timeout = 0.5
    ser.inter_byte_timeout = 0.5

    # UART_MUX.init()
    channels = [1,2]
    chnl_indx = 0
    while True:
        if chnl_indx > 2:
            chnl_indx = 0
        # inp = int(input("chanell :"))
        # UART_MUX.set_mux_uart(chnl_indx)
        chnl_indx += 1
        

        # send data 
        c_light_status  = [0x01, 0x87, 0x00, 0x00, 0x00, 0x00, 0x00]

        # Switch to active upload
        active_upload  = [0x01, 0x87, 0x0, 0x00, 0x00, 0x00, 0x00]
        command = [0xff] + active_upload + [parity_bit(active_upload)]
        print("Send:", command)
        ser.write(bytearray(command))

        # Check if incoming bytes are waiting to be read from the serial input 
        # buffer.
        # NB: for PySerial v3.0 or later, use property `in_waiting` instead of
        # function `inWaiting()` below!
        len_recive=17

        reentry = 500
        data = 0.0

        while(len_recive > 0):
            recived_bytes = ser.inWaiting() 
            if (reentry > 0):
                reentry -= 1
            else:
                break

            if (recived_bytes > 0):
                len_recive -= recived_bytes
                data = ser.read(recived_bytes) 
                # print the incoming string without putting a new-line
                # ('\n') automatically after every print()
                print(data, end='') 

            # Put the rest of your code you want here
            
            # Optional, but recommended: sleep 10 ms (0.01 sec) once per loop to let 
            # other threads on your PC run during this time. 
            time.sleep(0.001) 
        
        data_hex = data.hex()
        print("re-entry", reentry)
        print("data-hex", data_hex)
        print("data-hex", data)
        if(data == 0.0):
            print("Emepty")
        else:
            print([int(data_hex[i:i+2],16) for i in range(0, len(data_hex),2)])

def main():
    print("V1.00")
    fast_uart()
    exit()
    UART_MUX.init()
    cnt = 1
    inp = 1
    Ser_Client = serial.Serial( port='/dev/ttymxc1',)
    Ser_Client.baudrate = 9600
    Ser_Client.parity = 'N' 
    Ser_Client.stopbits = 1
    Ser_Client.bytesize = 8
    Ser_Client.timeout = 0.5
    Ser_Client.inter_byte_timeout = 0.5
    data = [0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xAA]
    s0 = Sensor(Ser_Client)
    while 1:
        #print("Count ", cnt)
        inp = int(input("chanell :"))
        print("Chanell is: ", inp)
        UART_MUX.set_mux_uart(inp)
        # Ser_Client.flushInput()

        get_data = s0.read_led_status()
        print(get_data)

        time.sleep(0.05)
        UART_MUX.set_mux_uart(9)
        cnt = cnt + 1
        if cnt > 9:
            cnt = 1

 
if __name__ == "__main__":
    main()
