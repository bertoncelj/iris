from threading import *
import time


def thread_1():

    for i in range(5):
        print("thread: ",i)
        time.sleep(3)


def hello():
    print("hello, world")

T = Thread(target = thread_1)
T.setDaemon(True)

t = Timer(1, hello)
t.start() 
T.start()
print(T.is_alive())
print("Main thread")
time.sleep(5)

