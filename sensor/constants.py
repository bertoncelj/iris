# -*- coding: utf-8 -*-

IMX6 = True
LED = False

VERSION = '0.1.10'
# ModBus/TCP
MODBUS_PORT = 502
# Modbus mode
MODBUS_TCP = 1
MODBUS_RTU = 2
# Modbus function code
# standard
READ_COILS = 0x01
READ_DISCRETE_INPUTS = 0x02
READ_HOLDING_REGISTERS = 0x03
READ_INPUT_REGISTERS = 0x04
WRITE_SINGLE_COIL = 0x05
WRITE_SINGLE_REGISTER = 0x06
WRITE_MULTIPLE_COILS = 0x0F
WRITE_MULTIPLE_REGISTERS = 0x10
MODBUS_ENCAPSULATED_INTERFACE = 0x2B
# Modbus except code
EXP_NONE = 0x00
EXP_ILLEGAL_FUNCTION = 0x01
EXP_DATA_ADDRESS = 0x02
EXP_DATA_VALUE = 0x03
EXP_SLAVE_DEVICE_FAILURE = 0x04
EXP_ACKNOWLEDGE = 0x05
EXP_SLAVE_DEVICE_BUSY = 0x06
EXP_NEGATIVE_ACKNOWLEDGE = 0x07
EXP_MEMORY_PARITY_ERROR = 0x08
EXP_GATEWAY_PATH_UNAVAILABLE = 0x0A
EXP_GATEWAY_TARGET_DEVICE_FAILED_TO_RESPOND = 0x0B
# Exception as short human readable
EXP_TXT = {
    EXP_NONE: 'no exception',
    EXP_ILLEGAL_FUNCTION: 'illegal function',
    EXP_DATA_ADDRESS: 'illegal data address',
    EXP_DATA_VALUE: 'illegal data value',
    EXP_SLAVE_DEVICE_FAILURE: 'slave device failure',
    EXP_ACKNOWLEDGE: 'acknowledge',
    EXP_SLAVE_DEVICE_BUSY: 'slave device busy',
    EXP_NEGATIVE_ACKNOWLEDGE: 'negative acknowledge',
    EXP_MEMORY_PARITY_ERROR: 'memory parity error',
    EXP_GATEWAY_PATH_UNAVAILABLE: 'gateway path unavailable',
    EXP_GATEWAY_TARGET_DEVICE_FAILED_TO_RESPOND: 'gateway target device failed to respond'
}
# Exception as details human readable
EXP_DETAILS = {
    EXP_NONE: 'The last request produced no exceptions.',
    EXP_ILLEGAL_FUNCTION: 'Function code received in the query is not recognized or allowed by slave.',
    EXP_DATA_ADDRESS: 'Data address of some or all the required entities are not allowed or do not exist in slave.',
    EXP_DATA_VALUE: 'Value is not accepted by slave.',
    EXP_SLAVE_DEVICE_FAILURE: 'Unrecoverable error occurred while slave was attempting to perform requested action.',
    EXP_ACKNOWLEDGE: 'Slave has accepted request and is processing it, but a long duration of time is required. '
                     'This response is returned to prevent a timeout error from occurring in the master. '
                     'Master can next issue a Poll Program Complete message to determine whether processing is completed.',
    EXP_SLAVE_DEVICE_BUSY: 'Slave is engaged in processing a long-duration command. Master should retry later.',
    EXP_NEGATIVE_ACKNOWLEDGE: 'Slave cannot perform the programming functions. '
                              'Master should request diagnostic or error information from slave.',
    EXP_MEMORY_PARITY_ERROR: 'Slave detected a parity error in memory. '
                             'Master can retry the request, but service may be required on the slave device.',
    EXP_GATEWAY_PATH_UNAVAILABLE: 'Specialized for Modbus gateways, this indicates a misconfigured gateway.',
    EXP_GATEWAY_TARGET_DEVICE_FAILED_TO_RESPOND: 'Specialized for Modbus gateways, sent when slave fails to respond.'
}
# Module error codes
MB_NO_ERR = 0
MB_RESOLVE_ERR = 1
MB_CONNECT_ERR = 2
MB_SEND_ERR = 3
MB_RECV_ERR = 4
MB_TIMEOUT_ERR = 5
MB_FRAME_ERR = 6
MB_EXCEPT_ERR = 7
MB_CRC_ERR = 8
MB_SOCK_CLOSE_ERR = 9
# Module error as short human readable
MB_ERR_TXT = {
    MB_NO_ERR: 'no error',
    MB_RESOLVE_ERR: 'name resolve error',
    MB_CONNECT_ERR: 'connect error',
    MB_SEND_ERR: 'socket send error',
    MB_RECV_ERR: 'socket recv error',
    MB_TIMEOUT_ERR: 'recv timeout occur',
    MB_FRAME_ERR: 'frame format error',
    MB_EXCEPT_ERR: 'modbus exception occur',
    MB_CRC_ERR: 'bad CRC on receive frame',
    MB_SOCK_CLOSE_ERR: 'socket is closed'
}

TOTAL_TYPE_8 = 0xf8
FLOAT32 = 0x44
FLOAT64 = 0x88
UINT32 = 0x04
SINT32 = 0x14
SINT16 = 0x12
UINT16 = 0x02
SINT8 = 0x11
UINT8 = 0x01

STRING = 0xff

R_ADD = 0
R_FUN = 1
R_TYPE = 2
R_SIZE = 3
R_REG = 4
R_LOC = 5
R_OFF = 6
R_FAC = 8
R_NAME = 10

R_BAUD = 0
R_PARITY = 2
R_STOP_BIT = 3
R_BYT_SIZE = 4
R_TIMEOUT = 5
R_INTER_TIMEOUT = 7

LOC_STATUS = 1090
LOC_AVR_PPM = 1100
LOC_AVR_VOLTAGE = 1120
LOC_AVR_VOLTAGE_AUX = 1140
LOC_AVR_CURRENT = 1160
LOC_AVR_CURRENT_AUX = 1180
LOC_AVR_CURRENT_ZERO = 1200
LOC_AVR_CURRENT_ZERO_AUX = 1220
LOC_AVR_TEMPERATURE = 1240

LOC_FST_PPM = 1260
LOC_FST_VOLTAGE = 1280
LOC_FST_CURRENT = 1320
LOC_FST_CURRENT_ZERO = 1360
LOC_FST_TEMPERATURE = 1400


LOC_GROUP = 1420
LOC_GROUP_SIZE = 20
LOC_OFFSET_PPM = 0
LOC_OFFSET_VOLTAGE = 2
LOC_OFFSET_VOLTAGE_AUX = 4
LOC_OFFSET_CURRENT = 6
LOC_OFFSET_CURRENT_AUX = 8
LOC_OFFSET_CURRENT_ZERO = 10
LOC_OFFSET_CURRENT_ZERO_AUX = 12
LOC_OFFSET_TEMPERATURE = 14

STRUCT_PPM = 0
STRUCT_VOLTAGE = 8
STRUCT_VOLTAGE_AUX = 16
STRUCT_CURRENT = 24
STRUCT_CURRENT_AUX = 32
STRUCT_CURRENT_ZERO = 40
STRUCT_CURRENT_ZERO_AUX = 48
STRUCT_TEMPERATURE = 56

PPM = 0
VOLTAGE = 1
VOLTAGE_AUX = 2
CURRENT = 3
CURRENT_AUX = 4
CURRENT_ZERO = 5
CURRENT_ZERO_AUX = 6
TEMPERATURE = 7
STATUS = 8

#AIR PUMP CONSTANT
AIR_PUMP_REG = 100
PWM_ = 30000
PWM_PERIOD = PWM_ * 100
