import os.path
import time

PWM_RESET = False  # Whether GPIOs should be re-exported
PWM_PATH = "/sys/class/pwm/pwmchip0"
PWM_DIR_OUT = "out"
PWM_DIR_IN = "in"
PWM_VAL_HI = "1"
PWM_VAL_LO = "0"
PWM_CHAN = "/pwm0"  # GPIO2_I003
# pin28 _> GPIO1_09

BLINK_PERIOD = 500  # Blink period (milliseconds)
BLINK_DUTY = 0.25  # Blink duty cycle (fraction)


class pwm():

    def __init__(self):
        print('start')
        ### Initialize GPIO - optionally reset if already initialized

        ## Note: GPIOs which are already used in the drivers can not be controlled from sysfs,
        ## unless a driver explicitly exported that particular pins GPIO.

        # Open GPIO export & unexport files
        exportFile = open(PWM_PATH + '/export', 'w')
        #print(exportFile)

        unexportFile = open(PWM_PATH + '/unexport', 'w')
        #print(unexportFile)

        exportExists = os.path.isdir(PWM_PATH + PWM_CHAN)
        #print(exportExists)

        if exportExists and PWM_RESET:
            #print('exportExists and PWM_RESET')
            unexportFile.write(PWM_CHAN)
            unexportFile.flush()
        
        if not exportExists or PWM_RESET:
            #print('exportExists or PWM_RESET')
            exportFile.write('0')
            exportFile.flush()
            


        print('1')
        self.dirFile_PWM_period = open(PWM_PATH + PWM_CHAN + '/period', 'w')
        print('2')
        self.dirFile_PWM_duty_cycle = open(PWM_PATH + PWM_CHAN + '/duty_cycle', 'w')
        print('3')
        self.dirFile_PWM_enable = open(PWM_PATH + PWM_CHAN + '/enable', 'w')
        print('4')

    def period(self, per):
        #print("per:", per)
        self.dirFile_PWM_period.write("%d" % per)
        self.dirFile_PWM_period.flush()

    def duty_cycle(self, dut):
        self.dirFile_PWM_duty_cycle.write("%d" % dut)
        self.dirFile_PWM_duty_cycle.flush()

    def enable(self, en):  # 1 enable,  0 disable
        st = ('%d' % en)
        #print('en2')
        #print( st )
        #print(self.dirFile_PWM_enable)
        self.dirFile_PWM_enable.write(st)
        #print('en3')
        self.dirFile_PWM_enable.flush()
        #print('en4')



def control_PWM():
    period_in_ns = 300000
    try:
        print('PWM')
        p = pwm()

        while(1):
            print('W1')
            p.period(period_in_ns*10)
            print(period_in_ns*10)
            print('W2')
            p.enable(0)
            print('W3')
            p.duty_cycle(0)
            print('W4')
            #up frequency
            for i in range(5,9,1):
                d = period_in_ns*i
                print(period_in_ns*10)
                print(d)
                p.duty_cycle(d)
                p.enable(1)
                # Sleep for blink off duration
                time.sleep(1)
            #down frequency
            for i in range(1, 5, 1):
                d = (period_in_ns*10) - (period_in_ns*i)
                print(period_in_ns*10)
                print(d) 
                p.duty_cycle(d)
                p.enable(1)
                # Sleep for blink off duration
                time.sleep(1)
            p.enable(0)
            time.sleep(2)

    except OSError as err:
        print("OS error: {0}".format(err))
        pass

if __name__ == "__main__":
    control_PWM()
