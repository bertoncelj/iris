import serial
import struct
import sys
import CRC16
import time
import csv
import logging
import uart_mux
from store import *
import PWM
#from client_reg_manager import reload_serial_parameters

import modbus_client
#import modbus_server
#import TCP_server
#from client_reg_manager import *
from constants import *


def ini():
    """
    logging.basicConfig(filename='log.log',
                        encoding='utf-8', level=logging.DEBUG)
    """
    
    #ini measurments
    #print("ini_store_data") 
    #for i in range(16):
    #    modbus_client.save_data(-3, [ 0, 0],2, i*4)
    
    import_client_reg("registers.csv")
    
    # set communication
    #import_serial_settings("serial.csv")
    
    #set average
    set_average()
    set_info()
    uart_mux.UART_MUX.init()

    
    if __debug__:
        pass    
    else:
        set_pump()
        pass

def set_pump():
    if IMX6 :
        if(AIR_PUMP_REG == 100):
            pow = DataBank.settings[AIR_PUMP_REG][0]
            pow = 80
            p = PWM.pwm()
            p.period(PWM_PERIOD)
            p.enable(0)
            if pow == 0:
                return()
            if(pow > 0) and (pow < 101):
                p.duty_cycle(PWM_ * pow)
                p.enable(1)
    
    pass

def set_info():
    
    DataBank.set_holding_string(2, "Gas Sensor 8")  # Model Type
    DataBank.set_holding_string(30, "2200123") # MAIN BOARD info serial
    DataBank.set_holding_string(39, "ver1.04") # MAIN BOARD SW version
    DataBank.set_holding_string(44, "HW 1A")  # MAIN BOARD Hardware Reference

    DataBank.set_holding_string(60, "3A12B45C56")  # TORADEX info Serial Number
    

    pass


def set_average():
    cnt = 0
    epoch_time = int(time.time())
    for i in [73,76,76,79,79,82,82,85]: #sampels number by value
        per = DataBank.settings[i-1][0]
        dl = epoch_time % per
        nxt = (epoch_time - dl) + per
        for j in range(8):
            average[(cnt)] = [0.0, 0, DataBank.settings[i][0],per,nxt]
            cnt = cnt+1
    
    pass

def get_val(row):
    if(row[2] in ['UInt16', 'Int16','UInt32','Int32','Int8']):
        return int(row[1])
    elif(row[2] in ['Char']):
        return (row[1])
    elif (row[2] in ['Float32', 'Float64']):   
        return float(row[1])
    else:
        return row[1]  

def import_client_reg( csv_file_path):
    with open(csv_file_path) as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')

        for row in spamreader:
            row = list(map(str.strip, row))

            if len(row) == 0 or row[0].startswith("#"):
                # ignore all blank and comment lines
                continue

            if len(row) != 4:
                # print("Expected 4 elements row: ", row)
                continue

            #try:
            #add_Client_parameters( addr, row)
            val = get_val(row)
            DataBank.settings[int(row[0])] = val, row[2], row[3]
            #except Exception as e:
    
    DataBank.fill_Client_reg_memory()
    modbus_client.reload_client_serial_parameters()
    print("Successufly loaded file ", csv_file_path)

    return True


def import_serial_settings( csv_file_path):
    try:
        with open(csv_file_path) as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')

            #addr = 1000
            for row in spamreader:
                row = list(map(str.strip, row))

                if len(row) == 0 or row[0].startswith("#"):
                    # ignore all blank and comment lines
                    continue

                if len(row) != 4:
                    # print("Expected 4 elements row: ", row)
                    continue

                #try:
                #add_serial_parameters(addr,row)
                val = get_val(row)
                DataBank.settings[int(row[0])] = val, row[2], row[3]
                #except Exception as e:
                #    print("Error parsing row:", row, e)

                #addr += 20
            
        DataBank.fill_serial_reg_memory()
        modbus_client.reload_client_serial_parameters()


        print("Set communication parameters for Client and Server")
        #print_com_param()
        print('COMMAND >')
        return True
    except:
        print("Error reading settings. Communication set to default!!")
        print('COMMAND >')
    return False
"""
def out_ser(csv_file_path):
    with open(csv_file_path, 'w') as csvfile:
        for row in DataBank.settings:
            if (row < 2000) or (row > 2040):
                continue
            txt = ("{0:>5}; {1:>16}; {2:>10}; {3:>12}\r".format(
                row, DataBank.settings[row][0], DataBank.settings[row][1], DataBank.settings[row][2]))
            csvfile.write(txt )
    csvfile.close()
    print('file ->' + csv_file_path)
    print('COMMAND >')
"""

def out_reg(csv_file_path):
    with open(csv_file_path, 'w') as csvfile:
        for row in DataBank.settings:
            if (row < 1) or (row > 1000):
                continue
            txt = ("{0:>5}; {1:>16}; {2:>10}; {3:>12}\r".format(
                row, DataBank.settings[row][0], DataBank.settings[row][1], DataBank.settings[row][2]))
            csvfile.write(txt)
    csvfile.close()
    print('file ->' + csv_file_path)

def lst_ser():
    for row in DataBank.settings:
        if (row < 2000) or (row > 2040):
            continue
        print("{0:>5}; {1:>16}; {2:>10}; {3:>12}\r".format(
            row, DataBank.settings[row][0], DataBank.settings[row][1], DataBank.settings[row][2]))
    print('COMMAND >')


