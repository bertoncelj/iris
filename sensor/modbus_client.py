import serial
import struct
import sys
import CRC16
import time
import csv
import logging
import store
import utils
import init
import uart_mux
from store import DataBank
from constants import *

#from CRC16 import CalCRC16
ukazi = []

class query:
    add = 1
    fun = 3
    reg = 3000
    size = 2
    typ = "Int32"
    name = "Value"
    loc = 1000
    off = 0.0
    fac = 1.0


if IMX6:
    Ser_Client = serial.Serial(
        port='/dev/ttymxc1',
    )
else:
    Ser_Client = serial.Serial(
        port='COM4',
    )

def reload_client_serial_parameters():
    #print("settings", store.settings)[[
    if ((R_BAUD+200) in DataBank.settings):
        Ser_Client.baudrate = DataBank.settings[R_BAUD+200][0]
    if ((R_PARITY+200) in DataBank.settings):
        Ser_Client.parity = DataBank.settings[R_PARITY+200][0]
    if ((R_STOP_BIT+200) in DataBank.settings):
        Ser_Client.stopbits = DataBank.settings[R_STOP_BIT+200][0]
    if ((R_BYT_SIZE+200) in DataBank.settings):
        Ser_Client.bytesize = DataBank.settings[R_BYT_SIZE+200][0]
    if ((R_TIMEOUT+200) in DataBank.settings):
        Ser_Client.timeout = DataBank.settings[R_TIMEOUT+200][0]
    if ((R_INTER_TIMEOUT+200) in DataBank.settings):
        Ser_Client.inter_byte_timeout = DataBank.settings[R_INTER_TIMEOUT+200][0]
    uart_mux.Sensor(Ser_Client)




# byte array to hex string 
def h(b):
    if type(b) == str:
        return ''.join('{:02X} '.format(ord(x)) for x in b)
    else:
        return ''.join('{:02X} '.format(x) for x in b)

# byte array to hex string 
def h1(b):
    return ''.join('{:02X} '.format(str(x)) for x in b)

# hex string to byte array
def a(s):
    return [int(i, 16) for i in s.split(" ")]

# hex string to byte string list
def l(s):
    return bytes(a(s))

def writeStr(serial, s):
    serial.write((s))


def check_imported_address(address):
    if address == 12:
        if store.save_settings_flag == 2: #prevent endless loop
            store.save_settings_flag = 0
        else:
            store.save_settings_flag = 1
    if address == 100: #    air pump power
        init.set_pump()
        pass

pass

def writeModbus(serial, data, reg):
    # map naredi isto kot: ''.join(chr(i) for i in data)
    # data je tipa int list in ga zato moramo pretvoriti v array(to je v pythonu string)
   
    crc = CRC16.CalCRC16(data)
    data.append(crc % 256)
    data.append(crc // 256)

    time_start = time.time()
    if store.print_trafic > 0:
        print("Client Snd %04d: " % (reg) + h(data))
    #logging.debug("Sending:  " + h(data))
    serial.flushInput()
    serial.write(data)
    #print("Poslano:  " + h(data))
    #time_send = time.time()
    #print("Response: " +  h(serial.read(8)))
    #print("Duration: %f %f" % (time.time() - time_start, time.time() - time_send))

def writeCurrent(serial, value):

    #print("Sending value: " + str(value/5) + "V " + str(value) + "% " + str(value10k))
    data = [0x01, 0x03, value//256, value%256, 0x00,0x04]
    data = [0x01, 0x0B, 0xBA, 0x02, 0x00, 0x01]
    #       id    fun   add   add   len   len   

    writeModbus(serial, data)

def writeRegister(serial, par):
    #print("Sending value: " + str(value/5) + "V " + str(value) + "% " + str(value10k))
    #data = [0x01, 0x03, value//256, value%256, 0x00,0x04]
    #data = [0x01, 0x03, 0x0B, 0xBA, 0x00, 0x01]
    data = []
    data.append(par.add)
    data.append(par.fun)
    data.append(par.reg//256)
    data.append(par.reg%256)
    data.append(par.size//256)
    data.append(par.size%256)

    writeModbus(serial, data, par.reg)
    time.sleep(0.1)

def readModbus(serial):
    received_data = serial.read(80)
    if store.print_trafic > 0:
        print("Client Rec: " + h(received_data))
    #logging.debug("Received: " + h(received_data))

    return received_data


def decode_rec(data,reg):  #data->list
    #calCRC
    val = -1
    lnght = len(data)
    if lnght < 6:
        DataBank.set_words_4(reg.loc, [0, 0, 10])
        return 0
    crc = CRC16.CalCRC16(data[:-2])
    crc_chk = data[lnght-1]*256 + data[lnght-2]
    if crc != crc_chk:
        DataBank.set_words_4(reg.loc, [0, 0, 13])
        return 0
    #get value
    lnght = int(data[2])
    tmp = [0,0,0]
    if reg.typ == FLOAT32:
        if lnght == 4:
            tmp[0] = data[3]*256 + data[4]
            tmp[1] = data[5]*256 + data[6]
            tmp[2] = 1
        else:
            DataBank.set_words_4(reg.loc, [0, 0, 12])
            return 0
            #strhex = ''.join('{:02x}'.format(x) for x in tmp)
            #val = struct.unpack('!f', bytes.fromhex(strhex))[0]
    elif reg.typ == UINT16:
        if lnght == 2:
            tmp[0] = 0
            tmp[1] = data[3]*256 + data[4]
            tmp[2] = 1
            #val = int(''.join('{:02x}'.format(x) for x in tmp), 16)
        else:
            DataBank.set_words_4(reg.loc, [0, 0, 12])
            return 0

    elif reg.typ == UINT8:
        if lnght == 2:
            tmp[0] = 0
            tmp[1] = data[4]
            tmp[2] = 1
            #val = int(''.join('{:02x}'.format(x) for x in tmp), 16)
        else:
            DataBank.set_words_4(reg.loc, [0, 0, 12])
            return 0
        """
    elif reg.typ == STRING:
        if lnght < 16:
            for j in range(lnght/2): 
                tmp[j] = data[3+j]*256 + data[4+j]
            #val = int(''.join('{:02x}'.format(x) for x in tmp), 16)
        else:
            save_meas(-2, [0, 0], 2, reg.loc)
            return 0
        """
    else:
        return 0
    DataBank.set_words_4(reg.loc, tmp)
    return 1

def decode_memory(reg):
    st=''
    txt =''
    ind = (reg.loc)*2
    if(reg.typ == FLOAT32):
        tmp = store.meas_data[ind:ind+4]
        tmp_by = bytearray(tmp)
        fl = struct.unpack('>f', tmp_by)
        txt = ("%3.3f" % fl)
    elif(reg.typ == UINT32):
        tmp = store.meas_data[ind:ind+4]  
        tmp_by = bytearray(tmp)
        rez = int.from_bytes((tmp_by), 'big')
        txt = ("%d" % rez)
    elif(reg.typ == UINT16):
        tmp = store.meas_data[ind:ind+2]  
        tmp_by = bytearray(tmp)
        rez = int.from_bytes((tmp_by), 'big')
        txt = ("%d" % rez)
    elif(reg.typ == UINT8):
        tmp = store.meas_data[ind:ind+2]  
        tmp_by = bytearray(tmp)
        rez = int.from_bytes((tmp_by), 'big')
        txt = ("%d" % rez)
    
    return reg.name, txt


def izpis_rezultatov():
    Q = query()
    for set_merId in store.settings:
        set_mer = store.settings[set_merId]
        Q.add = int(set_mer[R_ADD])
        Q.fun = int(set_mer[R_FUN])
        Q.reg = int(set_mer[R_REG])
        Q.size = int(set_mer[R_SIZE])
        Q.name = (set_mer[R_NAME])
        Q.loc = int(set_mer[R_LOC])
        Q.typ = int(set_mer[R_TYPE])
        st, txt = decode_memory(Q)
        print(' %s =  %s' % (st, txt))



def get_string(reg,len):
    txt=""
    lst = DataBank.get_words_4(reg,len)
    for wrd in lst:
        i = (wrd >> 8)
        if(i>31)and(i<127):
            txt = txt + chr(wrd>>8)
            i = (wrd & 0x00FF)
            if(i>31)and(i<127):
                txt = txt + chr(wrd & 0x00FF)
        else:
            break
    return txt

def get_Int16(reg):
    lst = DataBank.get_words_4(reg,2)
    in16 = lst[0]
    return in16

def get_Int32(reg):
    lst = DataBank.get_words_4(reg,2)
    long = lst[1]*65536 + lst[0]
    return long

def get_Float32(reg):
    lst = DataBank.get_words_4(reg,2)
    long = lst[1]*65536 + lst[0]
    return utils.decode_ieee(long)

###
def izpis_mer_po_sezorjih():

    for cnt_sensor in range(0, 8):
        print(" *****  S E N S O R  %d.  ******"% cnt_sensor)
        #name
        txt = get_string((LOC_GROUP*cnt_sensor)+LOC_OFFSET_NAME, 8)
        print(" name = %s"%(txt))
        #ID
        i32 = get_Int32((LOC_GROUP*cnt_sensor)+LOC_OFFSET_ID)
        print(" ID = %d"%(i32))
        #status
        i16 = get_Int16((LOC_GROUP*cnt_sensor)+LOC_OFFSET_STATUS)
        print(" status = %d"%(  i16))
        #ppm
        f32 = get_Float32((LOC_GROUP * cnt_sensor)+LOC_OFFSET_PPM)
        print(" ppm = %0.3fppm"% ( f32))
        #voltage sens
        f32 = get_Float32((LOC_GROUP * cnt_sensor)+LOC_OFFSET_VOLTAGE)
        print(" voltage sens = %0.3fV"%(  f32))
        #voltage Aux
        f32 = get_Float32((LOC_GROUP * cnt_sensor)+LOC_OFFSET_VOLTAGE_AUX)
        print(" voltage Aux = %0.3fV"%(  f32))
        #current sens
        f32 = get_Float32((LOC_GROUP * cnt_sensor)+LOC_OFFSET_CURRENT)
        print(" current sens = %0.3fmA"%(  f32))
        #current Aux
        f32 = get_Float32((LOC_GROUP * cnt_sensor)+LOC_OFFSET_CURRENT_AUX)
        print(" current Aux = %0.3fmA"%(  f32))
        #current zerro sens
        f32 = get_Float32((LOC_GROUP * cnt_sensor)+LOC_OFFSET_CURRENT_ZERO)
        print(" current zerro sens = %0.3fmA" % (f32))
        #current Aux
        f32 = get_Float32((LOC_GROUP * cnt_sensor)+LOC_OFFSET_CURRENT_ZERO_AUX)
        print(" current zerro Aux = %0.3fmA" % (f32))
        #temperature
        f32 = get_Float32((LOC_GROUP * cnt_sensor)+LOC_OFFSET_TEMPERATURE)
        print(" temperature = %0.3f'C" % (f32))
        print(" ")
###

def check_rec(data, dol):  # data->list
    #calCRC
    lnght = len(data)
    
    if len(data) < dol+5:
        return [], False
    crc = CRC16.CalCRC16(data[:-2])
    crc_chk = data[lnght-1]*256 + data[lnght-2]
    if crc != crc_chk:
        return [], False
    #get value
    lnght = int(data[2])
    payload = data[3:3+dol]

    return payload,True


def convert_to_array(fl):
    a = struct.unpack("I", struct.pack("f", fl))[0]
    return utils.long_to_word(a)

def avr_cal_float( values, avr, loc):
    val=[0]*4
    val[2] =  values[0]
    val[3] =  values[1]
    val[0] =  values[2]
    val[1] =  values[3]
    tmp_by = bytearray(val)
    fl = struct.unpack('>f', tmp_by)[0]
    nbr = store.average[avr][2]  # nbr values to avg
    cnt = store.average[avr][1]  # count nbr values
    cnt = cnt + 1
    store.average[avr][0] = fl +  store.average[avr][0]
    if ( cnt >= nbr ):
        fl = store.average[avr][0]/nbr
        words = convert_to_array(fl)
        DataBank.set_words_4(loc, words)
        cnt = 0
        store.average[avr][0] = 0.0
    
    store.average[avr][1] = cnt
    pass

#calculate average two float together
def avr_cal_2_float(value, avr, loc):
    val = [0]*4
    for i in [0, 1]:
        tmp_by = value[1+(i*2):5+(i*2)]
        val[2] = tmp_by[0]
        val[3] = tmp_by[1]
        val[0] = tmp_by[2]
        val[1] = tmp_by[3]
        tmp_by = bytearray(val)
        fl = struct.unpack('>f', tmp_by)[0]
        nbr = store.average[avr+i][2]  # nbr values to avg
        cnt = store.average[avr+i][1]  # count nbr values
        cnt = cnt + 1
        store.average[avr+i][0] = fl + store.average[avr][0]
        if (cnt >= nbr):
            fl = store.average[avr+i][0]/nbr
            words = convert_to_array(fl)
            DataBank.set_words_4(loc+(i*2), words)
            cnt = 0
            store.average[avr+i][0] = 0.0

        store.average[avr+i][1] = cnt
    pass


def read_status( address, size):
    Q = query()
    # Membrapor 1
    Q.add = 1
    Q.fun = 3
    Q.reg = address
    Q.size = size
    loc = LOC_STATUS
    writeRegister(Ser_Client, Q)
    rec = list(readModbus(Ser_Client))
    pay_load, status = check_rec(rec, size*2)
    if(status):
        DataBank.set_bytes_4(loc, pay_load)  # fast location
        store.sensor_status[0:4] = pay_load[1::2]

    # Membrapor 2
    Q.add = 2
    loc = LOC_STATUS + 4
    writeRegister(Ser_Client, Q)
    rec = list(readModbus(Ser_Client))
    pay_load, status = check_rec(rec, size*2)
    if(status):
        DataBank.set_bytes_4(loc, pay_load)  # fast location
        store.sensor_status[4:8]= pay_load[1::2]
    pass   



def read_meas_group(type,addres,size):
    Q = query()
    Q.add = 1
    Q.fun = 3
    Q.reg = addres
    Q.size = size
    struc = store.loc_value[type][0]
    loc_avr = store.loc_value[type][1]
    loc_fst = store.loc_value[type][2]
    loc_off = store.loc_value[type][3]
    writeRegister(Ser_Client, Q)
    rec = list(readModbus(Ser_Client))
    #print(rec)
    pay_load, status = check_rec(rec, size*2)
    #print(pay_load)
    if(status):
        for i in [0, 2, 4, 6]: # sensors from 1-4
            #save to fast
            value = pay_load[(i*2)+0:(i*2)+4]
            #print(value)
            if store.sensor_status[i>>1] == 3 :
                DataBank.set_bytes_4(loc_fst+i, value) #fast location
                loc = (int)(LOC_GROUP + (LOC_GROUP_SIZE*i/2) + loc_off)
                DataBank.set_bytes_4(loc, value)  # group sensor location
                avr_cal_float(value, struc+(i>>1), loc_avr+i)
            else:
                #measure sensor is offline, it sends float measure 0xffffffff -> NaN
                value =[0xff] * 4
                DataBank.set_bytes_4(loc_fst+i, value) #fast location
                loc = (int)(LOC_GROUP + (LOC_GROUP_SIZE*i/2) + loc_off)
                DataBank.set_bytes_4(loc, value)  # group sensor location
            #cal avg
            
    else:
        #read data is not valid, error in comunication, CRC wrong, or missing data, wrong data -> NaN
        for i in [0, 2, 4, 6]: # sensors from 1-4
            #save to fast
            value =[0xff] * 4
            #print(value)
            DataBank.set_bytes_4(loc_fst+i, value) #fast location
            loc = (int)(LOC_GROUP + (LOC_GROUP_SIZE*i/2) + loc_off)       

    Q.add = 2
    writeRegister(Ser_Client, Q)
    rec = list(readModbus(Ser_Client))
    pay_load, status = check_rec(rec, size*2)
    if(status):
        for i in [0, 2, 4, 6]:
            #save to fast
            value = pay_load[(i*2)+0:(i*2)+4]
            if store.sensor_status[(i>>1)+4] == 3 :
                DataBank.set_bytes_4(loc_fst+i+8, value) #fast location
                loc = (int)(LOC_GROUP + (LOC_GROUP_SIZE*(i+8)/2) + loc_off)
                DataBank.set_bytes_4(loc, value)  # group sensor location
                #cal avg
                avr_cal_float(value, struc+(i>>1)+4, loc_avr+i+8)
            else:
                value =[0xff] * 4
                DataBank.set_bytes_4(loc_fst+i+8, value) #fast location
                loc = (int)(LOC_GROUP + (LOC_GROUP_SIZE*(i+8)/2) + loc_off)
                DataBank.set_bytes_4(loc, value)  # group sensor location
    else:
        #measure sensor is offline, it sends float measure 0xffffffff -> NaN
        for i in [0, 2, 4, 6]:
            #save to fast
            value =[0xff] * 4
            DataBank.set_bytes_4(loc_fst+i+8, value) #fast location
            loc = (int)(LOC_GROUP + (LOC_GROUP_SIZE*(i+4)/2) + loc_off)

    epoch_time = int(time.time())
    val = [0]*2
    val[0] = epoch_time & 0xFFFF
    val[1] = epoch_time >> 16
    DataBank.set_words_4(loc_fst+16, val)  # fast location
    DataBank.set_words_4(loc_avr+16, val)  # avr location


#cal two float value together
def read_meas_group_dup(type, address, size):
    Q = query()
    Q.add = 1
    Q.fun = 3
    Q.reg = address
    Q.size = size
    struc = store.loc_value[type][0]
    loc_avr = store.loc_value[type][1]
    loc_fst = store.loc_value[type][2]
    loc_off = store.loc_value[type][3]
    writeRegister(Ser_Client, Q)
    rec = list(readModbus(Ser_Client))
    pay_load, status = check_rec(rec, size*2)
    if(status):
        for i in [0, 4, 8, 12]:
            #save two floats to fast mem 
            value = pay_load[(i*2)+0:(i*2)+8]
            if store.sensor_status[i>>2] == 3 :
                DataBank.set_bytes_4(loc_fst+i, value)  # fast location
                loc = (int)(LOC_GROUP + (LOC_GROUP_SIZE*i/4) + loc_off)
                DataBank.set_bytes_4(loc, value)  # group sensor location
                #cal avg
                avr_cal_2_float(value, struc+(i >> 1), loc_avr+i)
            else:
                value =[0xff] * 8
                DataBank.set_bytes_4(loc_fst+i, value)  # fast location
                loc = (int)(LOC_GROUP + (LOC_GROUP_SIZE*i/4) + loc_off)
                DataBank.set_bytes_4(loc, value)  # group sensor location
    else:
        for i in [0, 4, 8, 12]:
            value =[0xff] * 8
            DataBank.set_bytes_4(loc_fst+i, value)  # fast location
            loc = (int)(LOC_GROUP + (LOC_GROUP_SIZE*i/4) + loc_off)
            DataBank.set_bytes_4(loc, value)  # group sensor location    

    Q.add = 2
    writeRegister(Ser_Client, Q)
    rec = list(readModbus(Ser_Client))
    pay_load, status = check_rec(rec, size*2)
    if(status):
        for i in [0, 4, 8, 12]:
            #save to fast
            value = pay_load[(i*2)+0:(i*2)+8]
            if store.sensor_status[(i>>2)+4] == 3 :
                DataBank.set_bytes_4(loc_fst+i+16, value)  # fast location
                loc = (int)(LOC_GROUP + (LOC_GROUP_SIZE*(i+16)/4) + loc_off)
                DataBank.set_bytes_4(loc, value)  # group sensor location
                #cal avg
                avr_cal_2_float(value, struc+(i >> 1)+4, loc_avr+i+16)
            else:
                value =[0xff] * 8
                DataBank.set_bytes_4(loc_fst+i+16, value)  # fast location
                loc = (int)(LOC_GROUP + (LOC_GROUP_SIZE*(i+16)/4) + loc_off)
                DataBank.set_bytes_4(loc, value)  # group sensor location
    else:
        for i in [0, 4, 8, 12]:
            value =[0xff] * 8
            DataBank.set_bytes_4(loc_fst+i+16, value)  # fast location
            loc = (int)(LOC_GROUP + (LOC_GROUP_SIZE*(i+16)/4) + loc_off)
            DataBank.set_bytes_4(loc, value)  # group sensor location


    epoch_time = int(time.time())
    val = [0]*2
    val[0] = epoch_time & 0xFFFF
    val[1] = epoch_time >> 16
    DataBank.set_words_4(loc_fst+32, val)  # fast location
    DataBank.set_words_4(loc_avr+32, val)  # avr location




def read_config_group(off, addres, size):
    Q = query()
    Q.add = 1
    Q.fun = 3
    Q.reg = addres
    Q.size = size
    len = size/2
    writeRegister(Ser_Client, Q)
    rec = list(readModbus(Ser_Client))
    pay_load, status = check_rec(rec, size*2)
    if(status):
        # save by sensor groups
            st = 0
            for i in [0, 1, 2, 3]:
                en = int(st + size/2)
                DataBank.set_bytes_4(off+(LOC_GROUP*i), pay_load[st:en])
                st = en

    Q.add = 2
    writeRegister(Ser_Client, Q)
    rec = list(readModbus(Ser_Client))
    pay_load, status = check_rec(rec, size*2)
    if(status):
        if(status):
            # save by sensor groups
            st = 0
            for i in [4, 5, 6, 7]:
                en = int(st + size/2)
                DataBank.set_bytes_4(off+(LOC_GROUP*i), pay_load[st:en])
                st = en
            



# **************   main loop  **************************
def read_meas():
    #status
    
    
    for i in [1]:
        print("Chanell: ", i)
        print("uart mux", uart_mux)
        uart_mux.UART_MUX.set_mux_uart(i)
        get_data = uart_mux.Sensor.read_ppm()
        print("ppm", get_data)
        get_data = uart_mux.Sensor.read_temp()
        print("temp,hum", get_data)
    
    # """
    epoch_time = int(time.time())
    #ppm
    if ( store.average[STRUCT_PPM][4] <=  epoch_time):
        #a = time.gmtime(epoch_time)
        #print("ppm")
        read_status(1740, 4)
        read_meas_group(PPM, 1000, 8)
        #create next date
        dl = epoch_time % store.average[STRUCT_PPM][3]
        store.average[STRUCT_PPM][4] = (
            epoch_time - dl) + store.average[STRUCT_PPM][3]
    
    #voltage
    if (store.average[STRUCT_VOLTAGE][4] <= epoch_time):
        #print("voltage")
        read_meas_group_dup(VOLTAGE, 1010, 16)
        dl = epoch_time % store.average[STRUCT_VOLTAGE][3]
        store.average[STRUCT_VOLTAGE][4] = (epoch_time - dl) + store.average[STRUCT_VOLTAGE][3]

    #current
    if (store.average[STRUCT_CURRENT][4] <= epoch_time):
        read_meas_group_dup(CURRENT, 1030, 16)
        dl = epoch_time % store.average[STRUCT_CURRENT][3]
        store.average[STRUCT_CURRENT][4] = (
            epoch_time - dl) + store.average[STRUCT_CURRENT][3]
    
    #current zero
    if (store.average[STRUCT_CURRENT_ZERO][4] <= epoch_time):
        read_meas_group_dup(CURRENT_ZERO, 1050, 16)
        dl = epoch_time % store.average[STRUCT_CURRENT_ZERO][3]
        store.average[STRUCT_CURRENT_ZERO][4] = (
            epoch_time - dl) + store.average[STRUCT_CURRENT_ZERO][3]
    
    #temperature
    if (store.average[STRUCT_TEMPERATURE][4] <= epoch_time):
        read_meas_group(TEMPERATURE, 1070, 8)
        dl = epoch_time % store.average[STRUCT_TEMPERATURE][3]
        store.average[STRUCT_TEMPERATURE][4] = (
            epoch_time - dl) + store.average[STRUCT_TEMPERATURE][3]
    # """

def read_configuration():
    #read_config_group(LOC_OFFSET_NAME,40,8*4)
    #read_config_group(LOC_OFFSET_ID, 30, 2*4)
    pass

def reload_settings():
    if(DataBank.settings[12][0] == 1):  # SAVE
        init.out_reg("registers.csv")
        
    elif(DataBank.settings[12][0] == 2):  # SAVE and RELOAD
        init.out_reg("registers.csv")
        init.ini()
    
    store.save_settings_flag = 2  
    DataBank.settings[12] = 0, DataBank.settings[12][1], DataBank.settings[12][2]
    DataBank.fill_Client_reg_memory()
    
    pass



def modb_client():
    #print('Start Client ModBus')
    if (store.print_trafic == 1):
        store.print_trafic = 2
    if (store.save_settings_flag == 1):
        reload_settings()

    read_meas()
    read_configuration()
    
    if (store.print_trafic == 2):
        store.print_trafic = 0
        print('COMMAND >')
  


if __name__ == "__main__":
    modb_client()
