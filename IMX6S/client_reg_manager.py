import serial
import struct
import sys
import CRC16
import time
import csv
import logging
import share_store
import modbus_client
import modbus_server
from constants import *



def set_Client_reg(reg, data, len):
    if not ((reg > 99) and (reg < 1200)):
        return False
    #reg = reg - 100
    mod = reg % 20
    if(mod in [0, 1, 2, 3, 4, 5]) and (len == 2):
        val = data[0]*256 + data[1]
        share_store.reg_def[reg][0] = val
        return val

    elif(mod in [6, 8]) and (len == 4):
        aa = bytearray(data)
        share_store.reg_def[reg][0] = struct.unpack('<f', aa)
        return struct.unpack('>f', aa)
        
    elif (mod == 10) and (len < 16):
        txt =""
        for i in data:
            txt += chr(i)
            #txt = ''.join(data).decode('utf-8') 
        share_store.reg_def[reg][0] = txt
        return txt
    else:
        return False


def set_serial_reg(reg, data, len):
    if not ((reg > 999) and (reg < 1025)):
        return False
    reg = reg - 2000
    mod = reg % 20
    
    if mod == 0 and (len == 4):
        val = data[0]*256*256*256 + data[1]*256*256 + data[2]*256 + data[3]
        share_store.reg_def[reg][0]  = val
        return val

    elif(mod in [ 2, 3, 4]) and (len == 2):
        val = data[0]*256 + data[1]
        share_store.reg_def[reg][0]  = val
        return val
    
    else:
        return False




def fill_Client_reg_memory():
    #fill with 0
    for reg in range(0,1000):
        share_store.client_reg_mem.append ( 0 )

    for reg in range (100,800):
        if( reg in share_store.reg_def):
            typ = share_store.reg_def[reg][1]
            if(typ in ['UInt16', 'Int16','Int8']):
                share_store.client_reg_mem[reg-100] = int(share_store.reg_def[reg][0])
            elif(type in ['UInt32','Int32']):
                share_store.ser_mem[reg-100] = int(((share_store.reg_def[reg][0]) >> 16) & 0xFFFF)
                share_store.ser_mem[(reg-100) + 1] = int((share_store.reg_def[reg][0]) & 0xFFFF)
            elif (typ in ['Float32', 'Float64']):   
                val = float(share_store.reg_def[reg][0])
                a = list(struct.pack('<f', val))
                share_store.client_reg_mem[reg-100]   = a[3]*256 + a[2]
                share_store.client_reg_mem[(reg+1)-100] = a[1]*256 + a[0]
            else:
                txt = share_store.reg_def[reg][0]
                if len(txt) % 2 == 1:
                    txt += "\0"
                for ind in range (0,len(txt)>>1):
                    share_store.client_reg_mem[(reg + ind)-100] = ord(txt[(ind*2)]) * 256 + ord(txt[(ind*2)+1])


def fill_serial_reg_memory():
    #fill with 0
    for reg in range(0,40):
        share_store.ser_mem.append ( 0 )

    for reg in range (2000,2040):
        if( reg in share_store.reg_def):
            typ = share_store.reg_def[reg][1]
            if(typ in ['UInt16', 'Int16','Int8']):
                share_store.ser_mem[reg-2000] = int(share_store.reg_def[reg][0])
            elif(typ in ['Char']):
                # int(share_store.reg_def[reg][0])
                share_store.ser_mem[reg - 2000] = ord(share_store.reg_def[reg][0])
            elif (typ in ['UInt32', 'Int32']):
                share_store.ser_mem[reg-2000] = int(((share_store.reg_def[reg][0]) >> 16) & 0xFFFF)
                share_store.ser_mem[(reg-2000) + 1] = int((share_store.reg_def[reg][0]) & 0xFFFF)
            elif (typ in ['Float32', 'Float64']):   
                val = float(share_store.reg_def[reg][0])
                a = list(struct.pack('<f', val))
                share_store.ser_mem[(reg-2000)]   = a[3]*256 + a[2]
                share_store.ser_mem[(reg-2000)+1] = a[1]*256 + a[0]
            else:
                txt = share_store.reg_def[reg][0]
                if len(txt) % 2 == 1:
                    txt += "\0"
                for ind in range (0,len(txt)>>1):
                    share_store.ser_mem[(reg - 2000) + ind] = ord(txt[(ind*2)]) * 256 + ord(txt[(ind*2)+1])

def out_reg(csv_file_path):
    with open(csv_file_path, 'w') as csvfile:
        for  row in share_store.reg_def:
            if (row < 100) or (row > 1000):
                continue
            txt = (f"{row:>5}; {share_store.reg_def[row][0]:>16}; {share_store.reg_def[row][1]:>10}; {share_store.reg_def[row][2]:>12}\r" )
            csvfile.write(txt )
    csvfile.close()
    print('file ->' + csv_file_path)

"""


def fill_serial_reg_memory():
    #fill with 0
    for reg in range(0,40):
        share_store.ser_mem.append ( 0 )

    for ser in range (0,1):
        add = 20 * ser
        
        ch = 0  #32 bit baudrate
        if( 1000+add+ch in share_store.reg_def):
            share_store.ser_mem [add+ch] = int(((share_store.reg_def[1000+add+ch]) >> 16) & 0xFFFF)
            share_store.ser_mem[add+ch+1] = int((share_store.reg_def[1000+add+ch]) & 0xFFFF)
        
        ch = 2  #char
        if( 1000+add+ch in share_store.reg_def):
            share_store.ser_mem [add+ch] = ord(share_store.reg_def[1000+add+ch])
                
        for ch in [3,4]: #16bit 
            if( 1000+add+ch in share_store.reg_def):
                share_store.ser_mem [add+ch] = int(share_store.reg_def[1000+add+ch])

        for ch in [5,7]: #Float 
            if(1000+add+ch in share_store.reg_def):
                val = float(share_store.reg_def[1000+add+ch])
                a = list(struct.pack('<f', val))
                share_store.ser_mem[add+ch]   = a[3]*256 + a[2]
                share_store.ser_mem[add+ch+1] = a[1]*256 + a[0]

"""
# read from memory array -> client_reg_mem
def get_Client_reg_val( adr, len):
    answ = []
    adr = adr - 100
    for i in range (len):
        answ.append((share_store.client_reg_mem[adr+i] >> 8) & 0xFF)
        answ.append(share_store.client_reg_mem[adr+i] & 0xFF)
    return True, answ

# read from memory array -> ser_mem
def get_Client_ser_val( adr, len): 
    answ = []
    adr = adr - 2000
    for i in range (len):
        answ.append((share_store.ser_mem[adr+i] >> 8) & 0xFF)
        answ.append(share_store.ser_mem[adr+i] & 0xFF)
    return True, answ

def reload_serial_parameters():
    #print("reg_def", share_store.reg_def)[[
    if ((R_BAUD+2000) in share_store.reg_def):
        modbus_client.Ser_Client.baudrate = share_store.reg_def[R_BAUD+2000][0]
    if ((R_PARITY+2000) in share_store.reg_def):
        modbus_client.Ser_Client.parity = share_store.reg_def[R_PARITY+2000][0]
    if ((R_STOP_BIT+2000) in share_store.reg_def):
        modbus_client.Ser_Client.stopbits = share_store.reg_def[R_STOP_BIT+2000][0]
    if ((R_BYT_SIZE+2000) in share_store.reg_def):
        modbus_client.Ser_Client.bytesize = share_store.reg_def[R_BYT_SIZE+2000][0]
    if ((R_TIMEOUT+2000) in share_store.reg_def):
        modbus_client.Ser_Client.timeout = share_store.reg_def[R_TIMEOUT+2000][0]
    if ((R_INTER_TIMEOUT+2000) in share_store.reg_def):
        modbus_client.Ser_Client.inter_byte_timeout = share_store.reg_def[R_INTER_TIMEOUT+2000][0]

    if ((R_BAUD+2010) in share_store.reg_def):
        modbus_server.Ser_Server.baudrate = share_store.reg_def[R_BAUD+2010][0]
    if ((R_PARITY+2010) in share_store.reg_def):
        modbus_server.Ser_Server.parity = share_store.reg_def[R_PARITY+2010][0]
    if ((R_STOP_BIT+2010) in share_store.reg_def):
        modbus_server.Ser_Server.stopbits = share_store.reg_def[R_STOP_BIT+2010][0]
    if ((R_BYT_SIZE+2010) in share_store.reg_def):
        modbus_server.Ser_Server.bytesize = share_store.reg_def[R_BYT_SIZE+2010][0]
    if ((R_TIMEOUT+2010) in share_store.reg_def):
        modbus_server.Ser_Server.timeout = share_store.reg_def[R_TIMEOUT+2010][0]
    if ((R_INTER_TIMEOUT+2010) in share_store.reg_def):
        modbus_server.Ser_Server.inter_byte_timeout = share_store.reg_def[R_INTER_TIMEOUT+2010][0]



def test_reg():
    for reg in range(100, 300):
        answ = share_store.client_reg_mem[reg-100]
        print(answ)
        pass
    print(set_Client_reg(1, [0, 2], 2))
    print(set_Client_reg(120, [0, 2], 2))
    print(set_Client_reg(141, [0, 3], 2))
    print(set_Client_reg(100, [0, 2], 2))
    print(set_Client_reg(200, [0, 2], 2))
    print(set_Client_reg(301, [0, 2], 2))
    print(set_Client_reg(106, [60, 135, 252, 185], 4))
    print(set_Client_reg(107, [60, 0, 2, 1], 4))
    print(set_Client_reg(108, [68, 154, 82, 37], 4))
    print(set_Client_reg(110, [97, 98, 99, 100, 101, 102], 6))
    print(set_Client_reg(112, [97, 98, 99, 100, 101, 102], 6))


"""
# se ne dela !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def get_Client_reg( reg ,len):
    #fill with 0
    if not ((reg > 99)  and (reg < 300)):
        return False
    
    reg = reg -100
    if not (reg in share_store.reg_def):
        return False
    
    mod = reg % 20
    if( mod in [0,1,2,3,4,5]):
        val = int(share_store.reg_def[reg])
        return [
            val // 256,
            val % 256
        ]
    elif( mod in [6,8]):
        val = float(share_store.reg_def[reg])
        a = list(struct.pack('<f', val))
        return [ int(a[3]), int(a[2]),int(a[1]),int(a[0])]
    elif (mod > 9) and ( mod < 17):
       pass
"""
