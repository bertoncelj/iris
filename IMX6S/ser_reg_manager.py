import serial
import struct
import sys
import CRC16
import time
import csv
import logging
import share_store 
from constants import *

class mbType:
    def __init__(self, dict, valueName, value):
        self.dict = dict
        self.valueName = valueName
        self.dict[valueName] = value
        self.val_length = 1


class mbTypeInt16(mbType):
    def __init__(self, dict, valueName, value):
        super().__init__(dict, valueName, int(value))
    

    def write(self, value: int, pos:int):
        assert(pos == 0)

        self.dict[self.valueName] = value

    def read(self, pos):
        assert(pos == 0)

        value: int = self.dict[self.valueName]
        return [
            value // 256,
            value % 256
        ]


class mbTypeInt32(mbType):
    def __init__(self, dict, valueName, value):
        super().__init__(dict, valueName, int(value))

    def write(self, value: int, pos: int):
        assert(pos == 0)

        self.dict[self.valueName] = value

    def read(self, pos):
        assert(pos == 0)

        value: int = self.dict[self.valueName]
        return [
            (value >> 32) & 255,
            (value >> 16) & 255,
            (value >> 8 ) & 255,
            value & 255
        ]

class mbTypeFloat(mbType):
    def __init__(self, dict, valueName, value):
        super().__init__(dict, valueName, float(value))

    def write(self, value: float, pos: int):
        assert(pos == 0)

        self.dict[self.valueName] = value
"""
    def read(self, pos):
        assert(pos == 0)

        value: float = self.dict[self.valueName]
        return [
            value // 256,
            value % 256
        ]
"""
class mbTypeString (mbType):
    def __init__(self, dict, valueName, value):
        if len(value) % 2 == 1:
            value += "\0"

        super().__init__(dict, valueName, value)

        self.val_length = len(value)/2 # value lengh in registers

    def write(self, value: int, pos: int):
        assert(pos == 0)

    def read(self, pos):
        stringValue = self.dict[self.valueName]
        assert(pos < len(stringValue)/2+1)

        return [
            ord(stringValue[pos*2]),
            ord(stringValue[pos*2+1])
        ]


class mbTypeChar (mbType):
    def __init__(self, dict, valueName, value):
        super().__init__(dict, valueName, value)

        self.val_length = 1  # value lengh in registers

    def write(self, value: str, pos: int):
        assert(pos == 0)

    def read(self, pos):
        stringValue = self.dict[self.valueName]
        assert(pos < len(stringValue)/2+1)

        return [
            ord(stringValue[pos*2]),
            ord(stringValue[pos*2+1])
        ]

class ModBusManager:
    def __init__(self):
        self.mb_storage = {}

    def addParameter(self, mb_address: int, type_class: mbType):
        if self.areAddressFree(mb_address, type_class):
            self.mb_storage[mb_address] = type_class
            #print(self.mb_storage)
        else:
            pass
            #print("Address on location ", mb_address, " is not free!")

    def areAddressFree(self, address, storage_value: mbType):
        storage_addres_prev = None

        for storage_addres in sorted(self.mb_storage):
            if storage_addres == address:
                return False

            if storage_addres > address:
                lowOverlap = self.isLowerOverlap(storage_addres_prev, address)
                upOverlap = address + storage_value.val_length > storage_addres

                return not upOverlap and not lowOverlap
            
            storage_addres_prev = storage_addres

        return not self.isLowerOverlap(storage_addres_prev, address)

    def isLowerOverlap(self, addr_low, addr):
        # checks if lower address: add_low overlaps with the address: addr
        if addr_low is None:
            return False

        storage_value_prev = self.mb_storage[addr_low]
        return addr_low + storage_value_prev.val_length < addr
        

    def findStorageAddres(self, adr):
        storage_addres_prev = None

        for storage_addres in sorted(self.mb_storage):
            if storage_addres == adr:
                return adr

            if storage_addres > adr:
                return storage_addres_prev
            
            storage_addres_prev = storage_addres

        return storage_addres_prev

    def read(self, adr):
        storage_adr = self.findStorageAddres(adr)
        if storage_adr is None:
            return [0, 0]

        pos = adr - storage_adr  # position / offset

        storage_value = self.mb_storage[storage_adr]
        if storage_value.val_length <= pos:
            return [0, 0]

        return storage_value.read(pos)
    
    def readLen(self, adr, length):
        a = []

        for i in range(length):
            a.extend(self.read(adr + i))

        return a


    def printStorate(self):
        pass
    

#def addParameter(dict, valueName, value):
#    dict[valueName] = value


R_BAUD = "baud_rate"
R_PARITY = "parity"
R_STOP_BIT = "stop_bit"
R_BYT_SIZE = "byte_size"
R_TIMEOUT = "timeout"
R_INTER_TIMEOUT = "inter_byte_timeout"

# msr - measurement
def dodajSerialParemetri(mb_manager, mod_bus_address, csvRow):
    # doda vrednosti meritev v reg_def
    msrValues = {} # the values are filled insied mbTypeXXXX constructors
    portName = csvRow[0]
    share_store.serial[portName] = msrValues

    adr = mod_bus_address
    mb_ser_manager.addParameter(adr, mbTypeInt32(msrValues, R_BAUD, csvRow[1]))

    adr += 2
    mb_ser_manager.addParameter(
        adr, mbTypeChar(msrValues, R_PARITY, csvRow[2]))
    
    adr += 1
    mb_ser_manager.addParameter(
        adr, mbTypeInt16(msrValues, R_STOP_BIT, csvRow[3]))
    
    adr += 1
    mb_ser_manager.addParameter(
        adr, mbTypeInt16(msrValues, R_BYT_SIZE, csvRow[4]))
    
    adr += 1
    mb_ser_manager.addParameter(
        adr, mbTypeFloat(msrValues, R_TIMEOUT, csvRow[5]))
    
    adr += 1
    mb_ser_manager.addParameter(adr, mbTypeFloat( msrValues, R_INTER_TIMEOUT, csvRow[6]))

mb_ser_manager = ModBusManager()
