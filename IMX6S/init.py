import serial
import struct
import sys
import CRC16
import time
import csv
import logging
import share_store
import modbus_client
import modbus_server
from client_reg_manager import *
from constants import *


def ini():
    """
    logging.basicConfig(filename='log.log',
                        encoding='utf-8', level=logging.DEBUG)
    """
    print("ini_store_data")
    for i in range(16):
        modbus_client.save_data(-3, [ 0, 0],2, i*4)
    
    import_client_reg("registers.csv")
    # set communication
    import_serial_settings("serial.csv")

def get_val(row):
    if(row[2] in ['UInt16', 'Int16','UInt32','Int32','Int8']):
        return int(row[1])
    elif(row[2] in ['Char']):
        return (row[1])
    elif (row[2] in ['Float32', 'Float64']):   
        return float(row[1])
    else:
        return row[1]  

def import_client_reg( csv_file_path):
    with open(csv_file_path) as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')

        for row in spamreader:
            row = list(map(str.strip, row))

            if len(row) == 0 or row[0].startswith("#"):
                # ignore all blank and comment lines
                continue

            if len(row) != 4:
                print("Expected 4 elements row: ", row)
                continue

            #try:
            #add_Client_parameters( addr, row)
            val = get_val(row)
            share_store.reg_def[int(row[0])] =  val,row[2],row[3]
            #except Exception as e:

    fill_Client_reg_memory()
    #print("reg_def", share_store.reg_def)

    return True


def import_serial_settings( csv_file_path):
    try:
        with open(csv_file_path) as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')

            #addr = 1000
            for row in spamreader:
                row = list(map(str.strip, row))

                if len(row) == 0 or row[0].startswith("#"):
                    # ignore all blank and comment lines
                    continue

                if len(row) != 4:
                    print("Expected 4 elements row: ", row)
                    continue

                #try:
                #add_serial_parameters(addr,row)
                val = get_val(row)
                share_store.reg_def[int(row[0])] =  val,row[2],row[3]
                #except Exception as e:
                #    print("Error parsing row:", row, e)

                #addr += 20
        fill_serial_reg_memory()
        reload_serial_parameters()


        print("Set communication parameters for Client and Server")
        print_com_param()
        return True
    except:
        print("Error reading settings. Communication set to default!!")

    return False

def out_ser(csv_file_path):
    with open(csv_file_path, 'w') as csvfile:
        for  row in share_store.reg_def:
            if (row < 2000) or (row > 2040):
                continue
            txt = (f"{row:>5}; {share_store.reg_def[row][0]:>16}; {share_store.reg_def[row][1]:>10}; {share_store.reg_def[row][2]:>12}\r" )
            csvfile.write(txt )
    csvfile.close()
    print('file ->' + csv_file_path)

"""
def import_serial_settings(ime):
    #podatki = []
    try:
        with open(ime) as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')
            #print("1: ", spamreader)
            i = 0
            for row in spamreader:
                row = list(map(str.strip, row))
                i = i + 1
                if (i==1):
                    share_store.ser_client.baudrate = int(row[1])
                    share_store.ser_client.parity = row[2]
                    share_store.ser_client.stopbits = int(row[3])
                    share_store.ser_client.bytesize = int(row[4])
                    share_store.ser_client.timeout = float(row[5])
                    share_store.ser_client.inter_byte_timeout = float(row[6])
                elif (i==2):
                    share_store.ser_server.baudrate = int(row[1])
                    share_store.ser_server.parity = row[2]
                    share_store.ser_server.stopbits = int(row[3])
                    share_store.ser_server.bytesize = int(row[4])
                    share_store.ser_server.timeout = float(row[5])
                    share_store.ser_server.inter_byte_timeout = float(row[6])
                #print(', '.join(row))
"""     


def print_com_param():
    print("Client port: " + modbus_client.Ser_Client.port)
    print("Client baudrate: " + str(modbus_client.Ser_Client.baudrate))
    print("Client parity: " + modbus_client.Ser_Client.parity)
    print("Client stopbits: " + str( modbus_client.Ser_Client.stopbits))
    print("Client bytesize: " + str(modbus_client.Ser_Client.bytesize))
    print("Client timeout: " + str(modbus_client.Ser_Client.timeout))
    print("Client inter_byte_timeout: " +
          str(modbus_client.Ser_Client.inter_byte_timeout))
    
    print("Server port: " + modbus_server.Ser_Server.port)
    print("Server baudrate: " + str(modbus_server.Ser_Server.baudrate))
    print("Server parity: " + modbus_server.Ser_Server.parity)
    print("Server stopbits: " + str(modbus_server.Ser_Server.stopbits))
    print("Server bytesize: " + str(modbus_server.Ser_Server.bytesize))
    print("Server timeout: " + str(modbus_server.Ser_Server.timeout))
    print("Server inter_byte_timeout: " + str(modbus_server.Ser_Server.inter_byte_timeout))


