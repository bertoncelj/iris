import serial
import struct
import sys
import CRC16
import time
import csv
import logging
import share_store 
from constants import *

class mbType:
    def __init__(self, dict, valueName, value,len):
        self.dict = dict
        self.valueName = valueName
        self.dict[valueName] = value
        self.val_length = len


class mbTypeInt16(mbType):
    def __init__(self, dict, valueName, value,len):
        super().__init__(dict, valueName, int(value),len)
    

    def write(self, value: int, pos:int):
        assert(pos == 0)

        self.dict[self.valueName] = value

    def read(self, pos):
        assert(pos == 0)

        value: int = self.dict[self.valueName]
        return [
            value // 256,
            value % 256
        ]


class mbTypeInt32(mbType):
    def __init__(self, dict, valueName, value,len):
        super().__init__(dict, valueName, int(value),len)

    def write(self, value: int, pos: int):
        assert(pos == 0)

        self.dict[self.valueName] = value

    def read(self, pos):
        assert(pos == 0)

        value: int = self.dict[self.valueName]
        return [
            (value >> 24) & 0xFF,
            (value >> 16) & 0xFF,
            (value >>  8) & 0xFF,
            (value ) & 0xFF
        ]


class mbTypeFloat32(mbType):
    def __init__(self, dict, valueName, value,len):
        super().__init__(dict, valueName, float(value),len)

    def write(self, value: float, pos: int):
        assert(pos == 0)

        self.dict[self.valueName] = value

    def read(self, pos):
        assert(pos == 0)

        #value: float = self.dict[self.valueName]
        a = list(struct.pack('<f', self.dict[self.valueName]))  # little-endian
        
        return a

class mbTypeString (mbType):
    def __init__(self, dict, valueName, value):
        if len(value) % 2 == 1:
            value += "\0"

        super().__init__(dict, valueName, value,len)

        self.val_length = len(value)/2 # value lengh in registers

    def write(self, value: int, pos: int):
        assert(pos == 0)

    def read(self, pos):
        stringValue = self.dict[self.valueName]
        assert(pos < len(stringValue)/2+1)

        return [
            ord(stringValue[pos*2]),
            ord(stringValue[pos*2+1])
        ]

 

class ModBusManager:
    def __init__(self):
        self.mb_storage = {}
    """
    def addParameter(self, mb_address: int, type_class: mbType):
        if self.areAddressFree(mb_address, type_class):
            self.mb_storage[mb_address] = type_class
            #print(self.mb_storage)
        else:
            pass
            #print("Address on location ", mb_address, " is not free!")
    """
    def addParameter(self, mb_address: int, type_class: mbType):
        self.mb_storage[mb_address] = type_class
        #print(self.mb_storage)
        #print("Address on location ", mb_address, " is not free!")

    def areAddressFree(self, address, storage_value: mbType):
        storage_addres_prev = None

        for storage_addres in sorted(self.mb_storage):
            if storage_addres == address:
                return False

            if storage_addres > address:
                lowOverlap = self.isLowerOverlap(storage_addres_prev, address)
                upOverlap = address + storage_value.val_length > storage_addres

                return not upOverlap and not lowOverlap
            
            storage_addres_prev = storage_addres

        return not self.isLowerOverlap(storage_addres_prev, address)

    def isLowerOverlap(self, addr_low, addr):
        # checks if lower address: add_low overlaps with the address: addr
        if addr_low is None:
            return False

        storage_value_prev = self.mb_storage[addr_low]
        return addr_low + storage_value_prev.val_length < addr
        

    def findStorageAddres(self, adr):
        storage_addres_prev = None

        for storage_addres in sorted(self.mb_storage):
            if storage_addres == adr:
                return adr

            if storage_addres > adr:
                return storage_addres_prev
            
            storage_addres_prev = storage_addres

        return storage_addres_prev

    def read(self, adr):
        storage_adr = self.findStorageAddres(adr)
        if storage_adr is None:
            return [0, 0]

        pos = adr - storage_adr  # position / offset

        storage_value = self.mb_storage[storage_adr]
        if storage_value.val_length <= pos:
            return [0, 0]

        return storage_value.read(pos)
    
    def readLen(self, adr, length):
        a = []

        for i in range(length):
            a.extend(self.read(adr + i))

        return a


    def printStorate(self):
        pass
    

#def addParameter(dict, valueName, value):
#    dict[valueName] = value




# msr - measurement
def dodajModbusParemetri(mb_manager, mod_bus_address, csvRow):
    # doda vrednosti meritev v reg_def
    msrValues = {} # the values are filled insied mbTypeXXXX constructors
    measurementName = csvRow[0]
    share_store.reg_def[measurementName] = msrValues

    adr = mod_bus_address
    mb_manager.addParameter(adr, mbTypeInt16(msrValues, R_ADD, csvRow[3],1))

    adr += 1
    mb_manager.addParameter(adr, mbTypeInt16(msrValues, R_FUN, csvRow[4],1))
    
    adr += 1
    value = int(csvRow[7].split("x")[1], 16)
    mb_manager.addParameter(adr, mbTypeInt16(msrValues, R_TYPE, value,1))
    
    adr += 1
    mb_manager.addParameter(adr, mbTypeInt16(msrValues, R_SIZE, csvRow[6],1))
    
    adr += 1
    mb_manager.addParameter(adr, mbTypeInt16(msrValues, R_REG, csvRow[5],1))
    
    adr += 1
    mb_manager.addParameter(adr, mbTypeInt16(msrValues, R_LOC, csvRow[2],1))

    adr += 1
    mb_manager.addParameter(adr, mbTypeFloat32(msrValues, R_OFF, csvRow[8],1))

    adr += 2
    mb_manager.addParameter(adr, mbTypeFloat32(msrValues, R_FAC, csvRow[9],2))
    
    adr += 2
    mb_manager.addParameter(adr, mbTypeString(msrValues, R_NAME, csvRow[1]))


mb_manager = ModBusManager()
