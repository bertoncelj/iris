#!/usr/bin/env python
 
import time
import os.path
 
GPIO_RESET    = False;  # Whether GPIOs should be re-exported
GPIO_PATH     = "/sys/class/gpio";
GPIO_DIR_OUT  = "out";
GPIO_DIR_IN  = "in";
GPIO_VAL_HI   = "1";
GPIO_VAL_LO   = "0";
GPIO_CHAN_LED1 = "35";   #GPIO2_I003 
GPIO_CHAN_LED2 = "34";   #GPIO2_I002   1*32+2 = 34
GPIO_CHAN_LED5 = "106";  #GPIO2_I003   3*32+10=106
GPIO_CHAN_SW1 = "132";   #GPIO5_I004   4*32+4 =132 
GPIO_CHAN_SW2 =  "11";   #GPI01_I011 
 
BLINK_PERIOD  = 500;  # Blink period (milliseconds)
BLINK_DUTY    = 0.25; # Blink duty cycle (fraction)
 
class LED():
    val_LED1 = None
    val_LED2 = None
    val_LED5 = None
    def __init__(self): 
        print('start')
        ### Initialize GPIO - optionally reset if already initialized
 
        ## Note: GPIOs which are already used in the drivers can not be controlled from sysfs, 
        ## unless a driver explicitly exported that particular pins GPIO.
 
        # Open GPIO export & unexport files
        exportFile = open(GPIO_PATH+'/export', 'w')
        print(exportFile)
        unexportFile = open(GPIO_PATH+'/unexport', 'w')
        print(unexportFile)
        # Unexport GPIO if it exists and GPIO_RESET is enabled
        exportExists1 = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_LED1)
        if exportExists1 and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_LED1)
            unexportFile.flush()
        
        exportExists2 = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_LED2)
        if exportExists2 and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_LED2)
            unexportFile.flush()

        
        exportExists5 = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_LED5)
        if exportExists5 and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_LED5)
            unexportFile.flush()    
 
        # Export GPIO
        if not exportExists1 or GPIO_RESET:
            exportFile.write(GPIO_CHAN_LED1)
            exportFile.flush()

        if not exportExists2 or GPIO_RESET:
            exportFile.write(GPIO_CHAN_LED2)
            exportFile.flush()  

        if not exportExists5 or GPIO_RESET:
            exportFile.write(GPIO_CHAN_LED5)
            exportFile.flush()      
 

        # Open GPIO direction file to set direction
        dirFileLED1  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_LED1+'/direction','w')
        dirFileLED2  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_LED2+'/direction','w')
        dirFileLED5  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_LED5+'/direction','w')
 
        # Set GPIO direction to "out"
        dirFileLED1.write(GPIO_DIR_OUT)
        dirFileLED1.flush()
        dirFileLED2.write(GPIO_DIR_OUT)
        dirFileLED2.flush()
        dirFileLED5.write(GPIO_DIR_OUT)
        dirFileLED5.flush()

 
        # Open GPIO value file to set value
        self.val_LED1 = open(GPIO_PATH+'/gpio'+GPIO_CHAN_LED1+'/value','w')
        self.val_LED2 = open(GPIO_PATH+'/gpio'+GPIO_CHAN_LED2+'/value','w')
        self.val_LED5 = open(GPIO_PATH+'/gpio'+GPIO_CHAN_LED5+'/value','w')
        # Loop indefinitely
    
    def LED1_ON(self):
        self.val_LED1.write(GPIO_VAL_HI)
        self.val_LED1.flush()
    def LED1_OFF(self):
        self.val_LED1.write(GPIO_VAL_LO)
        self.val_LED1.flush()
    
    def LED2_ON(self):
        self.val_LED2.write(GPIO_VAL_HI)
        self.val_LED2.flush()
    def LED2_OFF(self):
        self.val_LED2.write(GPIO_VAL_LO)
        self.val_LED2.flush()

    def LED5_ON(self):
        self.val_LED5.write(GPIO_VAL_HI)
        self.val_LED5.flush()
    def LED5_OFF(self):
        self.val_LED5.write(GPIO_VAL_LO)
        self.val_LED5.flush() 

class BUTTOM():
    val_SW1 = None
    val_SW2 = None
    def __init__(self): 
        print('start')
        ### Initialize GPIO - optionally reset if already initialized
 
        ## Note: GPIOs which are already used in the drivers can not be controlled from sysfs, 
        ## unless a driver explicitly exported that particular pins GPIO.
 
        # Open GPIO export & unexport files
        exportFile = open(GPIO_PATH+'/export', 'w')
        unexportFile = open(GPIO_PATH+'/unexport', 'w')

        # Unexport GPIO if it exists and GPIO_RESET is enabled
        exportExists1 = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_SW1)
        if exportExists1 and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_SW1)
            unexportFile.flush()
        
        exportExists2 = os.path.isdir(GPIO_PATH+'/gpio'+GPIO_CHAN_SW2)
        if exportExists2 and GPIO_RESET:
            unexportFile.write(GPIO_CHAN_SW2)
            unexportFile.flush()

   
 
        # Export GPIO
        if not exportExists1 or GPIO_RESET:
            exportFile.write(GPIO_CHAN_SW1)
            exportFile.flush()

        if not exportExists2 or GPIO_RESET:
            exportFile.write(GPIO_CHAN_SW2)
            exportFile.flush()  
 

        # Open GPIO direction file to set direction
        dirFileSW1  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_SW1+'/direction','w')
        dirFileSW2  = open(GPIO_PATH+'/gpio'+GPIO_CHAN_SW2+'/direction','w')

 
        # Set GPIO direction to "in"
        dirFileSW1.write(GPIO_DIR_IN)
        dirFileSW1.flush()
        dirFileSW2.write(GPIO_DIR_IN)
        dirFileSW2.flush()

 
        # Open GPIO value file to set value
        self.val_SW1 = open(GPIO_PATH+'/gpio'+GPIO_CHAN_SW1+'/value','r')
        self.val_SW2 = open(GPIO_PATH+'/gpio'+GPIO_CHAN_SW2+'/value','r')

    
    def SW1(self):
        s =''
        self.val_SW1.seek(0)
        s = self.val_SW1.read()
        return s

    def SW2(self):
        s =''
        self.val_SW2.seek(0)
        s = self.val_SW2.read()
        return s


def control_buttoms():   
    try:     
        led = LED()
        sw = BUTTOM()
        while True:
            print(sw.SW1())
            print(sw.SW2())
            if(sw.SW1() == '1'):
                led.LED1_ON()
            else:
                led.LED1_OFF()
            
            if(sw.SW2() == '1'):
                led.LED2_ON()
            else:
                led.LED2_OFF()

            # Sleep for blink off duration
            time.sleep(0.5)
            led.LED5_ON()
            time.sleep(0.5)
            led.LED5_OFF()
 
    except exception:
        exception.printStackTrace()



def main():
    control_buttoms()
 
    return
 
if __name__ == "__main__":
    main()