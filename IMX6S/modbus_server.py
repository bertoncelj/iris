import serial
import struct
import sys
import CRC16
import time
import csv
import logging
import share_store
from init import * 
from client_reg_manager import get_Client_reg_val
from client_reg_manager import set_Client_reg

MSG_ERROR = -1
CRC_ERROR = -2
FUN_ERROR = -3
ADD_ERROR = -4
DATA_ERROR = -5
LEN_ERROR = 2
REG_ERROR = 3
OK_MSG = 1


rec = []

Ser_Server = serial.Serial(
    #port='/dev/ttymxc1',
    port='COM7',
)

"""
    baudrate=19200,
    parity=serial.PARITY_EVEN,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=0
)
"""


def writeStr(serial, s):
    serial.write((s))


def Server_Read_Modbus(serial):
    try:
        tmp = (serial.read(20))
        rec = []
        if(len(tmp) > 0):
            rec = list(tmp)
            #rec.append(int.from_bytes(tmp,"big"))
            #print("Received: " + h(received_data))
            #logging.debug("Received: " + h(rec))
        return rec
    except:
        return rec 


def decode_rec(data):
    answ = []    
    #check lenght
    lnght = len(data)
    if lnght < 8:
        #save_data(-1, 0.0, reg.loc)
        return MSG_ERROR,answ
    
    #cal CRC
    crc = CRC16.CalCRC16(data, lnght-2)
    crc_chk = data[lnght-1]*256 + data[lnght-2]
    if crc != crc_chk:
        #save_data(-2, 0.0, reg.loc)
        return CRC_ERROR, answ
    

    #check function
    add = int(data[0])
    if add != 1:
        rec.clear()     #remove elements from list 
        #return ADD_ERROR, answ
    if data[1] == 3:
        status, answ = read_fun_3(data, lnght) 
        #return st, ans
    elif data[1] == 6:
        status, answ = write_singel_reg_fun_6(data, lnght) 
        #return st, ans
    elif data[1] == 16:
        status, answ = write_singel_reg_fun_6(data, lnght)
        #return st, ans
    else:
        status = DATA_ERROR

    
    return status, answ



def write_reg(address, value):

    return True

def write_singel_reg_fun_6(data, lnght):
    #check reg
    answ = []
    address = data[2]*256 + data[3]
    value = data[4]*256 + data[5]
    status = OK_MSG
    if ( address > 99 ) and ( address < 720 ):
        #print(set_Client_reg(110, [97, 98, 99, 100, 101, 102], 6)) 
        set_Client_reg(address, data[4: 6], 2)
        fill_Client_reg_memory()
          
    elif (address > 799) and (address < 1200):
        set_Client_reg(address, data[4: 6], 2)
        fill_Client_reg_memory()

    elif (address > 1999) and (address < 2040):
        set_serial_reg(address, data[4: 6], 2)
    else:
        status = OK_MSG

    # prepare answer
    if status < 0:
        for d in range(lnght):
            answ.append(data[d])

        answ[0] = data[0]
        answ[1] = 0x80 | data[1]
        crc = CRC16.CalCRC16(answ, lnght-2)
        answ[lnght-1] = crc // 256
        answ[lnght-2] = crc % 256
        rec.clear()
        return REG_ERROR, answ
    else:
        for d in range(lnght):
            answ.append(data[d])

        crc = CRC16.CalCRC16(answ, lnght-2)
        answ[lnght-1] = crc // 256
        answ[lnght-2] = crc % 256
        rec.clear()
       
    return OK_MSG, answ

    




def read_fun_3(data, lnght):
    #check reg
    answ = []
    reg = data[2]*256 + data[3]
    ln = data[4]*256 + data[5]

    status, pay_load = read_reg(reg, ln)

    if status < 0:

        for d in range(lnght):
            answ.append(data[d])

        answ[0] = data[0]
        answ[1] = 0x80 | data[1]
        crc = CRC16.CalCRC16(answ, lnght-2)
        answ[lnght-1] = crc // 256
        answ[lnght-2] = crc % 256
        rec.clear()
        return REG_ERROR, answ
    else:
 
        #prepare answ
        ln = ln*2
        answ_len = (ln) + 3
        answ.append( data[0])
        answ.append(data[1])
        answ.append(ln)
        for d in pay_load:
            answ.append(d)
            #i = int(share_store.dt[reg+d], 16)
            #answ.append(i // 256)
            #answ.append(i  % 256)
        
        #answ_len = len(pay_load) + 3
        crc = CRC16.CalCRC16(answ, answ_len)
        answ.append(crc % 256)
        answ.append(crc // 256)
        rec.clear()
    return OK_MSG, answ


def read_reg (reg,lng):
    answ = []
    if(lng > 32):
        return False, []
    # 0 ... 63 measuring values and status
    if(reg >= 0 and reg < 64): 
       for d in range(lng):
            ind = (reg*2)+(d*2)
            if(ind<64):      # is inside measuring values
                answ.append(share_store.meas_data[ind])
                answ.append(share_store.meas_data[ind+1])
            else:          # is outside measuring values
                return False, answ
    # 64 ... 95 measuring values and status
    elif(reg >= 64 and reg < 96):
        reg = reg - 64
        for d in range(lng):
            ind = (reg)+(d)
            if(ind < 32):      # is inside measuring values
                 answ.append(0)
                 answ.append(share_store.meas_status[ind])
            else:          # is outside measuring values
                return False, answ
    # 100 ... 300 client register settings 
    elif(reg > 99 and reg < 1200):
        return get_Client_reg_val (reg,lng)
        #answ = mb_manager.readLen(reg,lng)
        #status = True
        #status, answ = read_sensor_reg_set(reg, lng, answ)
        #return status, answ
    # 100 ... 300 communication settings
    elif(reg > 1999 and reg < 2040):
        return get_Client_ser_val (reg,lng)
        #status, answ = read_sensor_reg_set(reg, lng, answ)
        return status, answ
    else:
        return False, []
    return True, answ
    #check len

"""
def read_sensor_reg_set(reg, lng, answ):

    reg = reg - 100
    cnt = 0
    # 
    while(reg > 20):
        reg = reg - 20
        cnt = cnt + 1
    #
    while (lng > 0):    
        row = share_store.reg_def.[cnt]
        if (reg == 1):
            answ.append(0)
            answ.append(row.[1])
        elif (reg == 2):
            answ.append(0)
            answ.append(row.[2])
"""

def ini_store_data():
    logging.basicConfig(filename='log.log',
                        encoding='utf-8', level=logging.DEBUG)
    #for i in range(16):
    #    save_data(-3, 0.0, i*2)

# **************   main loop  **************************
    #time.sleep(1)
    print('Start Server ModBus')
    #ser = serial.Serial("/dev/ttyLP2", 19200)  # Open port with baud rate
    #ini_store_data()
    




def modb_server():
    
    rec = list(Server_Read_Modbus(Ser_Server))
        
    status, data = decode_rec(rec)
    if (status > 0):
        Ser_Server.write(data)
        if share_store.print_trafic:
            print("S ", rec)
            print("S ", data)
    time.sleep(0.01)  # read


if __name__ == "__main__":
    modb_server()
