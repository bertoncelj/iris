import serial
import struct
import sys
import CRC16
import time
import csv
import logging
import share_store 
from constants import *

#from CRC16 import CalCRC16
ukazi = []


class query:
    add = 1
    fun = 3
    reg = 3000
    size = 2
    typ = "Int32"
    name = "Value"
    loc = 1000
    off = 0.0
    fac = 1.0

Ser_Client = serial.Serial(
    #port='/dev/ttymxc2',
    port='COM4',
)
"""
    baudrate=19200,
    parity=serial.PARITY_EVEN,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=0.6,
    inter_byte_timeout = 0.1,
)
"""
# byte array to hex string 
def h(b):
    if type(b) == str:
        return ''.join('{:02X} '.format(ord(x)) for x in b)
    else:
        return ''.join('{:02X} '.format(x) for x in b)

# byte array to hex string 
def h1(b):
    return ''.join('{:02X} '.format(str(x)) for x in b)

# hex string to byte array
def a(s):
    return [int(i, 16) for i in s.split(" ")]

# hex string to byte string list
def l(s):
    return bytes(a(s))

def writeStr(serial, s):
    serial.write((s))

def writeModbus(serial, data, reg):
    # map naredi isto kot: ''.join(chr(i) for i in data)
    # data je tipa int list in ga zato moramo pretvoriti v array(to je v pythonu string)
   
    crc = CRC16.CalCRC16(data, len(data))
    data.append(crc % 256)
    data.append(crc // 256)

    time_start = time.time()
    if share_store.print_trafic > 0:
        print("Client Snd %04d: " % (reg) + h(data))
    #logging.debug("Sending:  " + h(data))
    serial.write(data)
    #print("Poslano:  " + h(data))
    #time_send = time.time()
    #print("Response: " +  h(serial.read(8)))
    #print("Duration: %f %f" % (time.time() - time_start, time.time() - time_send))

def writeCurrent(serial, value):

    #print("Sending value: " + str(value/5) + "V " + str(value) + "% " + str(value10k))
    data = [0x01, 0x03, value//256, value%256, 0x00,0x04]
    data = [0x01, 0x0B, 0xBA, 0x02, 0x00, 0x01]
    #       id    fun   add   add   len   len   

    writeModbus(serial, data)

def writeRegister(serial, par):
    #print("Sending value: " + str(value/5) + "V " + str(value) + "% " + str(value10k))
    #data = [0x01, 0x03, value//256, value%256, 0x00,0x04]
    #data = [0x01, 0x03, 0x0B, 0xBA, 0x00, 0x01]
    data = []
    data.append(par.add)
    data.append(par.fun)
    data.append(par.reg//256)
    data.append(par.reg%256)
    data.append(par.size//256)
    data.append(par.size%256)

    writeModbus(serial, data, par.reg)

def readModbus(serial):
    received_data = serial.read(20)
    if share_store.print_trafic > 0:
        print("Client Rec: " + h(received_data))
    #logging.debug("Received: " + h(received_data))
    return received_data


def decode_rec(data,reg):  #data->list
    #calCRC
    val = -1
    lnght = len(data)
    if lnght < 6:
        save_data(-1, [0,0],2, reg.loc)
        return 0
    crc = CRC16.CalCRC16(data, lnght-2)
    crc_chk = data[lnght-1]*256 + data[lnght-2]
    if crc != crc_chk:
        save_data(-2, [0, 0], 2, reg.loc)
        return 0
    #get value
    lnght = int(data[2])
    if reg.typ == TOTAL_TYPE_8:
        if lnght == 8:
            tmp = data[3:11]
        else:
            save_data(-2, [0, 0], 2, reg.loc)
            return 0
            #celi_del = int(''.join('{:02x}'.format(x) for x in tmp),16)
            #tmp = data[7:11]
            #dec = int(''.join('{:02x}'.format(x) for x in tmp), 16)
            #val = float(celi_del + dec/(1000*1000*1000))
    elif reg.typ == FLOAT32:
        if lnght == 4:
            tmp = data[3:7]
        else:
            save_data(-2, [0, 0], 2, reg.loc)
            return 0
            #strhex = ''.join('{:02x}'.format(x) for x in tmp)
            #val = struct.unpack('!f', bytes.fromhex(strhex))[0]
    elif reg.typ == UINT16:
        if lnght == 2:
            tmp = data[3:5]
            #val = int(''.join('{:02x}'.format(x) for x in tmp), 16)
        else:
            save_data(-2, [0, 0], 2, reg.loc)
            return 0

    elif reg.typ == UINT8:
        if lnght == 2:
            tmp = data[3:5]
            #val = int(''.join('{:02x}'.format(x) for x in tmp), 16)
        else:
            save_data(-2, [0, 0], 2, reg.loc)
            return 0
    elif reg.typ == STRING:
        if lnght == 16:
            tmp = data[3:5]
            #val = int(''.join('{:02x}'.format(x) for x in tmp), 16)
        else:
            save_data(-2, [0, 0], 2, reg.loc)
            return 0
    else:
        return 0

    save_data(1,tmp, lnght, reg.loc)

    return 1

def decode_memory(reg):
    st=''
    txt =''
    ind = (reg.loc)*2
    if(reg.typ == FLOAT32):
        tmp = share_store.meas_data[ind:ind+4]
        tmp_by = bytearray(tmp)
        fl = struct.unpack('>f', tmp_by)
        txt = ("%3.3f" % fl)
    elif(reg.typ == UINT32):
        tmp = share_store.meas_data[ind:ind+4]  
        tmp_by = bytearray(tmp)
        rez = int.from_bytes((tmp_by), 'big')
        txt = ("%d" % rez)
    elif(reg.typ == UINT16):
        tmp = share_store.meas_data[ind:ind+2]  
        tmp_by = bytearray(tmp)
        rez = int.from_bytes((tmp_by), 'big')
        txt = ("%d" % rez)
    elif(reg.typ == UINT8):
        tmp = share_store.meas_data[ind:ind+2]  
        tmp_by = bytearray(tmp)
        rez = int.from_bytes((tmp_by), 'big')
        txt = ("%d" % rez)
    
    return reg.name, txt

def save_data( status, val, lnght, rg):
    rg = rg
    rg = rg * 2
    if(rg < 0 or rg > 64):
       return 
    if status > 0:
        # is sensor value  
        if rg < 64:
            #st = hex(struct.unpack('<I', struct.pack('<f', val))[0])
            for i in range(lnght):
                share_store.meas_data[rg+i] = val[i]
            share_store.meas_status[rg] = 0x01  # uspesno

    else:
        if rg < 64:
            share_store.meas_status[rg] = 0xFF  # neuspesno
            

def izpis_registrov():
    for cnt_sensor in range(0,16):
        addr = (cnt_sensor * 20) + 100
        if not ((addr + R_ADD) in share_store.reg_def):
            continue
        if not ((addr + R_FUN) in share_store.reg_def):
            continue
        if not ((addr + R_REG) in share_store.reg_def):
            continue
        if not ((addr + R_SIZE) in share_store.reg_def):
            continue
        if not ((addr + R_NAME) in share_store.reg_def):
            continue
        if not ((addr + R_LOC) in share_store.reg_def):
            continue
        if not ((addr + R_TYPE) in share_store.reg_def):
            continue
        if not ((addr + R_OFF) in share_store.reg_def):
            continue
        if not ((addr + R_FAC) in share_store.reg_def):
            continue

        print(f'address :  { share_store.reg_def[addr + R_ADD]}', end=" ")
        print(f'  funct:  {share_store.reg_def[addr + R_FUN]}', end=" ")
        print(f'  register:  {share_store.reg_def[addr + R_REG]}')
        print(f'  size:  {share_store.reg_def[addr + R_SIZE]}', end=" ")
        print(f'  comment:  {share_store.reg_def[addr + R_NAME]}', end=" ")
        print(f'  location:  {share_store.reg_def[addr + R_LOC]}', end=" ")
        print(f'  type:  {share_store.reg_def[addr + R_TYPE]}', end=" ")
        print(f'  offset:  {share_store.reg_def[addr + R_OFF]}', end=" ")
        print(f'  factor:  {share_store.reg_def[addr + R_FAC]}')
        print(' ')
        

def izpis_rezultatov():
    Q = query()
    for set_merId in share_store.reg_def:
        set_mer = share_store.reg_def[set_merId]
        Q.add = int(set_mer[R_ADD])
        Q.fun = int(set_mer[R_FUN])
        Q.reg = int(set_mer[R_REG])
        Q.size = int(set_mer[R_SIZE])
        Q.name = (set_mer[R_NAME])
        Q.loc = int(set_mer[R_LOC])
        Q.typ = int(set_mer[R_TYPE])
        st, txt = decode_memory(Q)
        print(' %s =  %s' % (st, txt))


def out_mer(csv_file_path):
    Q = query()

    with open(csv_file_path, 'w') as csvfile:

        for cnt_sensor in range(0,16):
            addr = (cnt_sensor * 20) + 100
            if not ((addr + R_ADD) in share_store.reg_def):
                continue
            if not ((addr + R_FUN) in share_store.reg_def):
                continue
            if not ((addr + R_REG) in share_store.reg_def):
                continue
            if not ((addr + R_SIZE) in share_store.reg_def):
                continue
            if not ((addr + R_NAME) in share_store.reg_def):
                continue
            if not ((addr + R_LOC) in share_store.reg_def):
                continue
            if not ((addr + R_TYPE) in share_store.reg_def):
                continue
            if not ((addr + R_OFF) in share_store.reg_def):
                continue
            if not ((addr + R_FAC) in share_store.reg_def):
                continue

            Q.add = int(share_store.reg_def[addr + R_ADD][0])
            Q.fun = int(share_store.reg_def[addr + R_FUN][0])
            Q.reg = int(share_store.reg_def[addr + R_REG][0])
            Q.size = int(share_store.reg_def[addr + R_SIZE][0])
            Q.name = (share_store.reg_def[addr + R_NAME][0])
            Q.loc = int(share_store.reg_def[addr + R_LOC][0])
            Q.typ = int(share_store.reg_def[addr + R_TYPE][0])
            Q.off = float(share_store.reg_def[addr + R_OFF][0])
            Q.fac = float(share_store.reg_def[addr + R_FAC][0])            #print("podatek:", podatek)
            st, txt = decode_memory(Q)
            #print(' %s =  %s' % (st, txt))
            txt = (f"{st} = {txt}\r" )
            csvfile.write(txt )
    csvfile.close()       
    print('file ->' + csv_file_path)


def izpis_mer():
    Q = query()
    for cnt_sensor in range(0, 16):
        addr = (cnt_sensor * 20) + 100
        if not ((addr + R_ADD) in share_store.reg_def):
            continue
        if not ((addr + R_FUN) in share_store.reg_def):
            continue
        if not ((addr + R_REG) in share_store.reg_def):
            continue
        if not ((addr + R_SIZE) in share_store.reg_def):
            continue
        if not ((addr + R_NAME) in share_store.reg_def):
            continue
        if not ((addr + R_LOC) in share_store.reg_def):
            continue
        if not ((addr + R_TYPE) in share_store.reg_def):
            continue
        if not ((addr + R_OFF) in share_store.reg_def):
            continue
        if not ((addr + R_FAC) in share_store.reg_def):
            continue
        
        Q.add = int(share_store.reg_def[addr + R_ADD][0])
        Q.fun = int(share_store.reg_def[addr + R_FUN][0])
        Q.reg = int(share_store.reg_def[addr + R_REG][0])
        Q.size = int(share_store.reg_def[addr + R_SIZE][0])
        Q.name = (share_store.reg_def[addr + R_NAME][0])
        Q.loc = int(share_store.reg_def[addr + R_LOC][0])
        Q.typ = int(share_store.reg_def[addr + R_TYPE][0])
        Q.off = float(share_store.reg_def[addr + R_OFF][0])
        # print("podatek:", podatek)
        Q.fac = float(share_store.reg_def[addr + R_FAC][0])
        st, txt = decode_memory(Q)
        #print(' %s =  %s' % (st, txt))
        print(f"{st} = {txt} {Q.reg} {Q.typ} ")



# **************   main loop  **************************
def modb_client():
    #print('Start Client ModBus')
    
    Q = query()
    Q.add = 1
    Q.reg = 3000
    #share_store.print_trafic = 1
    if (share_store.print_trafic == 1 ):
        share_store.print_trafic = 2

    for cnt_sensor in range(0,16):
        addr = (cnt_sensor * 20) + 100
        if not ((addr + R_ADD) in share_store.reg_def):
            continue
        if not ((addr + R_FUN) in share_store.reg_def):
            continue
        if not ((addr + R_REG) in share_store.reg_def):
            continue
        if not ((addr + R_SIZE) in share_store.reg_def):
            continue
        if not ((addr + R_NAME) in share_store.reg_def):
            continue
        if not ((addr + R_LOC) in share_store.reg_def):
            continue
        if not ((addr + R_TYPE) in share_store.reg_def):
            continue
        if not ((addr + R_OFF) in share_store.reg_def):
            continue
        if not ((addr + R_FAC) in share_store.reg_def):
            continue

        Q.add = int(share_store.reg_def[addr + R_ADD][0])
        Q.fun = int(share_store.reg_def[addr + R_FUN][0])
        Q.reg = int(share_store.reg_def[addr + R_REG][0])
        Q.size = int(share_store.reg_def[addr + R_SIZE][0])
        Q.name = (share_store.reg_def[addr + R_NAME][0])
        Q.loc = int(share_store.reg_def[addr + R_LOC][0])
        Q.typ = int(share_store.reg_def[addr + R_TYPE][0])
        Q.off = float(share_store.reg_def[addr + R_OFF][0])
        Q.fac = float(share_store.reg_def[addr + R_FAC][0])            #print("podatek:", podatek)
        
        writeRegister(Ser_Client, Q)
        rec = list(readModbus(Ser_Client))
        decode_rec(rec,Q)
        #writeRegister(ser2, Q)
        #readModbus(ser2)
        time.sleep(0.1)  # send every 1 seconds
    
    if (share_store.print_trafic == 2):
        share_store.print_trafic = 0
    
    #izpis_registrov()
    #izpis_rezultatov()
    """
    for d in range(16):
        print("%d = %s   %s" % (d, str(share_store.dt[d]), str(share_store.dt[d+32])) )
        #logging.debug("%d = %s" % (d, str(share_store.dt[d])))
    """
    


if __name__ == "__main__":
    modb_client()
