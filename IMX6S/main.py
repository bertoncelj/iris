import sys
import time
from threading import Thread
import modbus_client
import modbus_server
import share_store
import init
import client_reg_manager 
from init import ini
from init import print_com_param
from init import out_ser
from modbus_client import out_mer
from client_reg_manager import out_reg
from constants import *

"""
class cntrl_led_butt (Thread):

    def __init__(self):
        Thread.__init__(self)
        self.setDaemon(True)
        #print('running mdb_cliet')
        self.led = led_buttom.LED()
        self.sw = led_buttom.BUTTOM()
        self.run_flag = True

    def run(self):
        print('running cntrl_buttom')
        while self.run_flag:
            led_buttom.cntrl_buttom(self.sw,self.led)
        print('end of cntrl_buttom')

    def stop(self):
        print('I am going to stop cntrl_buttom')
        self.run_flag = False
"""

class Client_ModBus(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.setDaemon(True)
        #print('running mdb_cliet')
        self.run_flag = True

    def run(self):
        print('running mdb_cliet')
        #if modbus_client.ini_store_data():
        while self.run_flag:
            #print(self.run_flag)
            modbus_client.modb_client()
            time.sleep(2)
        #else:
        #    print('end of mdb_cliet')

    def stop(self):
        print('I am going to stop modb_client')
        self.run_flag = False


class Server_ModBus(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.setDaemon(True)
        #print('running mdb_cliet')
        self.run_flag = True

    def run(self):
        print('running modb_server')
        while self.run_flag:
            modbus_server.modb_server()

        print('end of modb_server')

    def stop(self):
        print('I am going to stop modb_server')
        self.run_flag = False




class Terminal_Read(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.setDaemon(True)
        self.running = True

    def run(self):
        print('running read_input')
        while self.running:
            #time.sleep(1)
            Term_Read()

    def stop(self):
        self.running = False


def Term_Read():
    #print('Start Read_inp')
    #while True:
    try:
        commandFull = input("COMMAND > ")

        commandParts = commandFull.split()
        if len(commandParts) == 0:
            exit

        command = commandParts[0]
        args = commandParts[1:]

        print(command)
        
        if command == "?":
            print('Help je v izdelavi.')
            print('<clientstop>  ustavi delovanje branje senzorjev')
            print('<clientrun>  zazene delovanje branje senzorjev')
            print('<mer>  izpise prebrane vrednosti registrov senzorjev')
            print('<t>  prikaze promet branje registrov senzorjev v enem ciklu')
            print('<com?>  parametri komunikacije')
            print('<reg?>  registri senzorjev ')
            print('<outreg> generate registers setting file -> register.txt')
            print('<outser> generate communication setting file -> serial.txt')
            print('<outmer> generate measuring results file -> meas.txt')
        elif command == "clientstop":
            print('execute command stop')
            f_Mod_Client.stop()
        
        elif command == "clientrun":
            print('execute command stop')
            f_Mod_Client.run()
        
        elif command == "mer":
            modbus_client.izpis_mer()
        
        elif command == "t":
            share_store.print_trafic = 1

        elif command == "com?":
            print_com_param()

        elif command == "reg?":
            modbus_client.izpis_registrov()

        elif command == "outreg":
            out_reg('register.txt')    
        
        elif command == "outser":
            out_ser('serial.txt')
        
        elif command == "outmer":
            out_mer('meas.txt') 

        elif len(command) == 0:
            exit
        
        elif command == "echo":
            #print(args)
            for d in range(40):
                print("%d = %s" % (d, str(share_store.meas_data[d])))
        else:
            try:
                print(eval(commandFull))
            except Exception as e:
                print(e)
#print("Unknown command: ", command)

    except KeyboardInterrupt:
        print("Type exit to exit the program")
        sys.exit(1)



ini()

#print(share_store.reg_def)
f_Term_Read = Terminal_Read()
f_Mod_Server = Server_ModBus()
#f_Mod_Client = Client_ModBus()

f_Term_Read.start()
f_Mod_Server.start()
#f_Mod_Client.start()

while True:
    pass 

Print('Kar nehal sem ?')
