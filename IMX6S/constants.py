TOTAL_TYPE_8 = 0xf8
FLOAT32 = 0x44
FLOAT64 = 0x88
UINT32 = 0x04
SINT32 = 0x14
SINT16 = 0x12
UINT16 = 0x02
SINT8 = 0x11
UINT8 = 0x01

STRING = 0xff

"""
R_ADD = "add"
R_FUN = "fun"
R_SIZE = "size"
R_TYPE = "type"
R_REG = "reg"
R_NAME = "name"
R_LOC = "loc"
R_OFF = "off"
R_FAC = "fac"
"""
R_BAUD = 0   # "baud_rate"
R_PARITY = 2  # "parity"
R_STOP_BIT = 3   # "stop_bit"
R_BYT_SIZE = 4  # "byte_size"
R_TIMEOUT = 5  # "timeout"
R_INTER_TIMEOUT = 7  # "inter_byte_timeout"

R_ADD = 0
R_FUN = 1
R_TYPE = 2
R_SIZE = 3
R_REG = 4
R_LOC = 5
R_OFF = 6
R_FAC = 8
R_NAME = 10
