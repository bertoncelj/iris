import serial 
import sys
import libscrc
import time
import random



# byte array to hex string 
def h(b):
    return ''.join('{:02X} '.format(x) for x in b)

# hex string to byte array
def a(s):
    return [int(i, 16) for i in s.split(" ")]

# hex string to byte string list
def l(s):
    return bytes(a(s))

def writeStr(serial, s):
    serial.write(c(s))

def writeModbus(serial, data):
    crc = libscrc.modbus("\x01\x03\x0B\xB8\x00\x04")
    print("%0x" % crc, bytes(data), str(data))
    data.append(crc % 256)
    data.append(crc // 256)        

def writeCurrent(serial, value):
    
    #print("Sending value: " + str(value/5) + "V " + str(value) + "% " + str(value10k))
    data = [0x01, 0x03, value//256, value%256, 0x00, 0x04]
    writeModbus(serial, data)




writeCurrent(None, 3000)

















